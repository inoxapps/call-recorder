package com.call.recorder.pro.auto.recorder;

import java.io.File;
import java.io.IOException;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.widget.Toast;

import com.call.recorder.pro.auto.R;
import com.call.recorder.pro.auto.notification.NotificationHanlder;
import com.call.recorder.pro.auto.preference.PrefrenceManager;
import com.call.recorder.pro.auto.service.CallStateSeekService;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class CallRecorder {

	private MediaRecorder recorder = null;
	private Context context = null;
	private long startTime = 0L;
	private long stopTime = 0L;
	
	public final static int RECORDING_SOURCE_MIC = 1;
	public final static int RECORDING_SOURCE_UPLINK = 2;
	public final static int RECORDING_SOURCE_DOWNLINK = 3;

	public CallRecorder(Context context) {
		this.context = context;
	}
	
	public void startAndNotifiy(Context context, int audioSource, int outputFormat, int audioEncoder, String filename) throws IOException {
		AppLog.i("PrefrenceManager.isShowNotification(context) value : "+PrefrenceManager.isShowNotification(context));
		if(PrefrenceManager.isShowNotification(context)){
			int notificationid = 3;
			PackageManager pm = context.getPackageManager();
			Intent intent = pm.getLaunchIntentForPackage(context.getPackageName());
				PendingIntent pIntent = PendingIntent.getActivity(context.getApplicationContext(), notificationid,
						intent, PendingIntent.FLAG_CANCEL_CURRENT);
				NotificationHanlder.deliverNotification(context, notificationid, Utilities.getTotalRecordings(context)+1 + " calls recorded.", pIntent);
		}
		start(context, audioSource, outputFormat, audioEncoder, filename);
	}

	public void start(Context context, int audioSource, int outputFormat, int audioEncoder, String filename) throws IOException {
		recorder = new MediaRecorder();
		String path = PrefrenceManager.getDefaultPathForRecording(context);
		AppLog.i("Checking base path : "+path);
		if(path == null)
			path = Utilities.getSDPath(context);
		AppLog.i("Checking should file save with ext : "+PrefrenceManager.isSaveFileWithExt(context));
		if(PrefrenceManager.isSaveFileWithExt(context))
			filename = filename+".3gp";
		path = path + filename;
		File file = (new File(path)).getParentFile();
		if (!file.exists() && !file.mkdirs()) {
			AppLog.i("WARNING : throwing exception : Path to file could not be created.");
			throw new IOException("Path to file could not be created.");
		}
		if(CallStateSeekService.currentCall != null){
			CallStateSeekService.currentCall.setFilepath(path);
		}
		AppLog.i("PrefrenceManager.isShowNotification(context) checking for toast : "+PrefrenceManager.isShowNotification(context));
		if(PrefrenceManager.isShowNotification(context))
			Toast.makeText(context, "recorder started.", Toast.LENGTH_SHORT).show();
		
		AppLog.i("Check media recorder settings : "+audioSource+":"+outputFormat+":"+audioEncoder);
//		recorder.setAudioEncodingBitRate(16);
//		recorder.setAudioSamplingRate(44100);
		recorder.setAudioSource(audioSource);
		recorder.setOutputFormat(outputFormat);
		recorder.setAudioEncoder(audioEncoder);
		recorder.setOutputFile(path);
		recorder.prepare();
		recorder.start();
	}
	
	public void stopAndNotify() throws IllegalArgumentException, SecurityException, IllegalStateException, IOException{
		AppLog.i("PrefrenceManager.isShowNotification(context) checking for stop toast : "+PrefrenceManager.isShowNotification(context));
		if(PrefrenceManager.isShowNotification(context))
			Toast.makeText(context, "recorder stopped", Toast.LENGTH_SHORT).show();
		stop();
	}
	
	public void stop() throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		if (recorder != null) {
			try {
				recorder.stop();
				recorder.reset();
				recorder.release();
			} catch (RuntimeException e) {
				e.printStackTrace();
			}
			catch (Exception e) {
				e.printStackTrace();
			}
			finally{
				recorder = null;
			}
			
			if(CallStateSeekService.currentCall != null){
				// Here media player is using for getting recording file time.
				MediaPlayer playerGetTime = new MediaPlayer();
				playerGetTime.setDataSource(CallStateSeekService.currentCall.getFilepath());
				playerGetTime.prepare();
				AppLog.i("Duration in stop : "+ playerGetTime.getDuration());
				CallStateSeekService.currentCall.setDuration((long) playerGetTime.getDuration());
				playerGetTime.release();
				playerGetTime = null;
			}
		}
	}

	// XXX commented code is used for AudioRecord.

	// public void start(Context context) throws IOException {
	// // recorder = new MediaRecorder();
	// audiorecord = Utilities.getAudioRecordInstance();
	// String path = null;
	// if (Environment.getExternalStorageState().equals("mounted")) {
	// path = Environment.getExternalStorageDirectory().getAbsolutePath();
	// } else {
	// throw new IOException("Memory card not inserted.");
	// }
	// // path = path + "/temp.bin";
	// // Log.i(TAG, "path : " + path);
	// // File file = (new File(path)).getParentFile();
	// // if (!file.exists() && !file.mkdirs()) {
	// // Log.i(TAG,
	// // "throwing exception : Path to file could not be created.");
	// // throw new IOException("Path to file could not be created.");
	// // }
	// Toast.makeText(context, "recorder started.", Toast.LENGTH_SHORT).show();
	// // recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
	// // recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
	// // recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
	// // recorder.setOutputFile(path);
	// // recorder.prepare();
	// // recorder.start();
	// Utilities.startRecording(context, audiorecord, path);
	// }
	//
	// public void stop() {
	// String path = null;
	// path = Environment.getExternalStorageDirectory().getAbsolutePath();
	// path = path+"/testRecord.wav";
	// if (audiorecord != null) {
	// Toast.makeText(context, "recorder stopped", Toast.LENGTH_SHORT)
	// .show();
	// Utilities.stopAndSaveRecording(audiorecord, path);
	// // recorder.stop();
	// // recorder.reset();
	// // recorder.release();
	// // recorder = null;
	// }
	//
	// }

}
