package com.call.recorder.pro.auto;

import java.io.File;
import java.io.IOException;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.call.recorder.pro.auto.contactManager.ContactListActivity;
import com.call.recorder.pro.auto.directoryhelper.DirectoryChooserDialog;
import com.call.recorder.pro.auto.preference.PrefrenceManager;
import com.call.recorder.pro.auto.service.CallStateSeekService;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class SettingActivity extends PreferenceActivity implements
		OnPreferenceClickListener, OnPreferenceChangeListener {

	private final int PICKFILE_RESULT_CODE = 200;
	private String m_chosenDir = "";
	private String RATING = "rate_app";

	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.settingcontainer);
		addPreferencesFromResource(R.xml.settings);
		
		// ad mediation banner show.
		RelativeLayout rladview = (RelativeLayout) findViewById(R.id.rlsetAdView);
		AppLog.i("checking relative layout in setting activity: "+rladview);
		// AdMediation.showBanner(this, rladview);			
		
		try {
			String basepath = Utilities.getSDPath(getApplicationContext());
			File basefile = new File(basepath);
			if(!basefile.isDirectory()){
				basefile.mkdir();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		startService(new Intent(this, CallStateSeekService.class));

		boolean isSimReady = Utilities.isSimPresent(getApplicationContext());
		if (!isSimReady) {
			Toast.makeText(getApplicationContext(), "Sim state unknown.",
					Toast.LENGTH_LONG).show();
		}

		Preference opref = (Preference) findPreference(getApplicationContext()
				.getString(R.string.set_openwithpassword));
		opref.setOnPreferenceClickListener(this);

		Preference rpref = (Preference) findPreference(getApplicationContext()
				.getString(R.string.set_defaultpath));
		rpref.setOnPreferenceClickListener(this);
		
		Preference cpref = (Preference) findPreference(getApplicationContext()
				.getString(R.string.set_selectcontacts));
		cpref.setOnPreferenceClickListener(this);
		
//		Preference dpref = (Preference) findPreference(getApplicationContext().getString(R.string.set_deletealarm));
//		dpref.setOnPreferenceChangeListener(this);
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		AppLog.i("Prefrence clicked : " + preference.getKey());
		if (preference.getKey().equals(
				getApplicationContext()
						.getString(R.string.set_openwithpassword))) {
			final Dialog dialog = new Dialog(SettingActivity.this);
			dialog.setContentView(R.layout.entertextdialog);
			dialog.setTitle("Enter password.");
//			LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
//			RelativeLayout passview = (RelativeLayout) inflater.inflate(
//					R.layout.entertextdialog, null);
			final EditText passText = (EditText) dialog.findViewById(R.id.etPassText);
			Button okbtn = (Button) dialog.findViewById(R.id.btnOk);
			Button cancelbtn =(Button) dialog.findViewById(R.id.btnCancel);
			okbtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					String text = passText.getText().toString();
					if(text != null && text.length() > 3){
						PrefrenceManager.setAppWithPasswordStatus(getApplicationContext(), text);
						dialog.dismiss();
					}
					else
						Toast.makeText(getApplicationContext(), "Password length should be greater than 3.", Toast.LENGTH_LONG).show();
				}
			});
			cancelbtn.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					dialog.dismiss();
				}
			});
			dialog.setCanceledOnTouchOutside(true);
			dialog.show();
		} else if (preference.getKey().equals(
				getApplicationContext().getString(R.string.set_defaultpath))) {
            boolean m_newFolderEnabled = true;
			DirectoryChooserDialog directoryChooserDialog = 
	                new DirectoryChooserDialog(SettingActivity.this, 
	                    new DirectoryChooserDialog.ChosenDirectoryListener(){
	                    @Override
	                    public void onChosenDir(String chosenDir){
	                        m_chosenDir = chosenDir;
	                        PrefrenceManager.setDefaultPathForRecording(getApplicationContext(), m_chosenDir);
	                        Toast.makeText(
	                        		SettingActivity.this, "Chosen directory: " + 
	                          chosenDir, Toast.LENGTH_LONG).show();
	                    }
	                });
	                // Toggle new folder button enabling
	                directoryChooserDialog.setNewFolderEnabled(m_newFolderEnabled);
	                directoryChooserDialog.chooseDirectory(m_chosenDir);
		}
		else if(preference.getKey().equals(
				getApplicationContext().getString(R.string.set_selectcontacts))){
			startActivity(new Intent(this, ContactListActivity.class));
		}
		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		AppLog.i("resultCode : " + resultCode + " : request code : "
				+ requestCode);
		if (resultCode == RESULT_OK && requestCode == PICKFILE_RESULT_CODE) {
			if (data != null) {
				AppLog.i("checking data : " + data + " : "
						+ data.getDataString());
			} else {
				AppLog.i("ERROR : Result code for path selector : "
						+ resultCode + " : data : " + data);
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		if(preference.getKey().equals(getApplicationContext().getString(R.string.set_deletealarm))){
			AppLog.i("changed value  : "+newValue.toString());
		}
		return false;
	}

	@Override
	public void onBackPressed() {
//		SharedPreferences sharedPrefs = PreferenceManager
//		.getDefaultSharedPreferences(getApplicationContext());
//		AppLog.i("Back press rate check : "+sharedPrefs.getBoolean(RATING, true));
////		if (sharedPrefs.getBoolean(RATING, true)) {
////			Utilities.showRateDialoge(this);
////		}
////		else{
////			super.onBackPressed();
////		}
////		Utilities.showRateDialoge(this);
		super.onBackPressed();
	}
	
}
