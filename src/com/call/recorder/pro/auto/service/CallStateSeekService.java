package com.call.recorder.pro.auto.service;

import java.io.File;
import java.io.IOException;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.MediaRecorder;
import android.os.IBinder;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import com.call.recorder.pro.auto.database.ContactTable;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.preference.PrefrenceManager;
import com.call.recorder.pro.auto.recorder.CallRecorder;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class CallStateSeekService extends Service {

	public static ContactTable currentCall = null; // this variable is tracking
													// call recorder, is it
													// already started or not.
	private PhoneStateListener listener = null;
	private TelephonyManager telephonyManager = null;
	 private BroadcastReceiver outgoingReciever = null;
	private CallRecorder recorder = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		try {
			// checking basepath.
			String basepath = Utilities.getSDPath(getApplicationContext());
			File basefile = new File(basepath);
			if(!basefile.isDirectory()){
				basefile.mkdir();
			}
		} catch (IOException e) {
			currentCall = null;			// shows call stopped.
			e.printStackTrace();
		}
		
		telephonyManager = (TelephonyManager) getApplicationContext()
				.getSystemService(Context.TELEPHONY_SERVICE);
		
		listener = new PhoneStateListener(){
			@Override
			public void onCallStateChanged(int state, String incomingNumber) {
				switch(state){
				case TelephonyManager.CALL_STATE_RINGING:
					AppLog.i("CALL_STATE_RINGING");
					if(Utilities.checkValidNumber(incomingNumber)){
						if(PrefrenceManager.getAppStatus(getApplicationContext())){
							if(incomingNumber != null){
								AppLog.i("INCO : checking app mode : "+PrefrenceManager.getAppMode(getApplicationContext()));
								if(PrefrenceManager.getAppMode(getApplicationContext()) == PrefrenceManager.APP_MODE_SELECTED){
									DBManager manager = new DBManager();
									String info[] = Utilities.getContactInfoFromNumber(getApplicationContext(), incomingNumber);
									if(info == null)
										break;
									AppLog.i("contact information : "+info[0]+":"+info[1]);
									boolean isselected = manager.isContactSelected(getApplicationContext(), info[0]);
									if(!isselected){
										AppLog.i("ERROR : App mode selected and contact is not in database.");
										break;
									}
								}
								if(currentCall == null){			// stating call recorder if currentcall object is null which showing recording is not on.
									currentCall = new ContactTable();				// shows call started.
									currentCall.setContactnumber(incomingNumber);
									currentCall.setState(DBManager.CALL_TYPE_INCOMING);
									String info[] = Utilities.getContactInfoFromNumber(getApplicationContext(), incomingNumber);
									startRecording(info);
								}
								else
									AppLog.i("WARNING : call recorder already started.");
							}
						}
					}
					break;
				case TelephonyManager.CALL_STATE_OFFHOOK:
					AppLog.i("CALL_STATE_OFFHOOK");
					if(Utilities.checkValidNumber(incomingNumber)){
					if(PrefrenceManager.getAppStatus(getApplicationContext())){
						if(incomingNumber != null){
							AppLog.i("INCO : checking app mode : "+PrefrenceManager.getAppMode(getApplicationContext()));
							if(PrefrenceManager.getAppMode(getApplicationContext()) == PrefrenceManager.APP_MODE_SELECTED){
								DBManager manager = new DBManager();
								String info[] = Utilities.getContactInfoFromNumber(getApplicationContext(), incomingNumber);
								if(info == null)
									break;
								AppLog.i("contact information : "+info[0]+":"+info[1]);
								boolean isselected = manager.isContactSelected(getApplicationContext(), info[0]);
								if(!isselected){
									AppLog.i("ERROR : App mode selected and contact is not in database.");
									break;
								}
							}
							if(currentCall == null){			// stating call recorder if currentcall object is null which showing recording is not on.
								currentCall = new ContactTable();				// shows call started.
								currentCall.setContactnumber(incomingNumber);
								currentCall.setState(DBManager.CALL_TYPE_INCOMING);
								String info[] = Utilities.getContactInfoFromNumber(getApplicationContext(), incomingNumber);
								startRecording(info);
							}
							else
								AppLog.i("WARNING : call recorder already started.");
						}
					}
				}
					break;
				case TelephonyManager.CALL_STATE_IDLE:
					AppLog.i("CALL_STATE_IDLE");
					try {
						stopRecording();
					} catch (IllegalArgumentException e) {
						currentCall = null;
						e.printStackTrace();
					} catch (SecurityException e) {
						currentCall = null;
						e.printStackTrace();
					} catch (IllegalStateException e) {
						currentCall = null;
						e.printStackTrace();
					} catch (IOException e) {
						currentCall = null;
						e.printStackTrace();
					}
					break;
				}
			}
		};
		
		telephonyManager.listen(listener, PhoneStateListener.LISTEN_CALL_STATE);
		
		outgoingReciever = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				String action = intent.getAction();
				if(PrefrenceManager.getAppStatus(getApplicationContext())){
					if(action != null && action.equals(Intent.ACTION_NEW_OUTGOING_CALL)){
						String outgoingnumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
						if(Utilities.checkValidNumber(outgoingnumber)){
							AppLog.i("INCO : outgoing checking app mode : "+PrefrenceManager.getAppMode(getApplicationContext()));
							if(PrefrenceManager.getAppMode(getApplicationContext()) == PrefrenceManager.APP_MODE_SELECTED){
								DBManager manager = new DBManager();
								String info[] = Utilities.getContactInfoFromNumber(getApplicationContext(), outgoingnumber);
								if(info == null)
									return;
								AppLog.i("contact information : "+info[0]+":"+info[1]);
								boolean isselected = manager.isContactSelected(getApplicationContext(), info[0]);
								if(!isselected){
									AppLog.i("ERROR : App mode selected and contact is not in database.");
									return;
								}
							}
							if(currentCall == null){			// stating call recorder if currentcall object is null which showing recording is not on.
								AppLog.i("CALL_STATE_OUTGOING");
								currentCall = new ContactTable();				// shows call started.
								currentCall.setContactnumber(outgoingnumber);
								currentCall.setState(DBManager.CALL_TYPE_OUTGOING);
								String info[] = Utilities.getContactInfoFromNumber(getApplicationContext(), outgoingnumber);
								startRecording(info);
							}
							else
								AppLog.i("WARNING : call recorder already started.");
						}
					}
				}
			}
		};
		
		IntentFilter filter = new IntentFilter(Intent.ACTION_NEW_OUTGOING_CALL);
		this.registerReceiver(outgoingReciever, filter);
	}

	public void startRecording(String uinfo[]) {
		if (PrefrenceManager.getAppStatus(getApplicationContext())) {
			recorder = new CallRecorder(getApplicationContext());
			try {
				int source = 0;
				try {
					source = Integer.parseInt(PrefrenceManager
							.getRecordingSource(getApplicationContext()));
				} catch (Exception e) {
					source = CallRecorder.RECORDING_SOURCE_MIC;
					e.printStackTrace();
				}
				int audioSource = -1;
				int outputFormat = -1;
				int audioEncoder = -1;
				switch (source) {
				case 0: // considerin same CallRecorder.RECORDING_SOURCE_MIC for
						// 0.
				case CallRecorder.RECORDING_SOURCE_MIC:
					AppLog.i("Source for recording is MIC");
					audioSource = MediaRecorder.AudioSource.MIC;
					outputFormat = MediaRecorder.OutputFormat.THREE_GPP;
					audioEncoder = MediaRecorder.AudioEncoder.AMR_NB;
					break;
				case CallRecorder.RECORDING_SOURCE_UPLINK:
					AppLog.i("Source for recording is Uplink");
					audioSource = MediaRecorder.AudioSource.VOICE_UPLINK;
					outputFormat = MediaRecorder.OutputFormat.THREE_GPP;
					audioEncoder = MediaRecorder.AudioEncoder.AMR_WB;
					break;
				case CallRecorder.RECORDING_SOURCE_DOWNLINK:
					AppLog.i("Source for recording is Downlink");
					audioSource = MediaRecorder.AudioSource.VOICE_DOWNLINK;
					outputFormat = MediaRecorder.OutputFormat.THREE_GPP;
					audioEncoder = MediaRecorder.AudioEncoder.AMR_WB;
					break;
				default:
					AppLog.i("ERROR: Recording source error. source is "
							+ source);
					AppLog.i("INFO : Setting source back to default.");
					PrefrenceManager.setRecordingSource(
							getApplicationContext(),
							String.valueOf(CallRecorder.RECORDING_SOURCE_MIC));
					return;
				}

				String filename = null;
				if (uinfo == null) {
					AppLog.i("INFO : Contact id is null for contact number : "
							+ currentCall.getContactnumber());
					filename = currentCall.getContactnumber() + "_"
							+ System.currentTimeMillis();
				} else {
					AppLog.i("INFO : setting contact id : "
							+ Integer.parseInt(uinfo[0]));
					currentCall.setContactid(Integer.parseInt(uinfo[0]));
					filename = uinfo[2] + "_" + System.currentTimeMillis();
				}

				// checking recorder values
				if (audioSource < 0 || outputFormat < 0 || audioEncoder < 0) {
					AppLog.i("ERROR : AudioSource or OutputFormat or AudioEncoder is not setted means it is 0.");
					AppLog.i("ERROR : Making current call object null.");
					currentCall = null; // shows call stopped.
				} else {
					AppLog.i("INFO : Starting recording from service.");
					currentCall.setDate(Utilities.getDateInMilli());
					recorder.startAndNotifiy(getApplicationContext(),
							audioSource, outputFormat, audioEncoder, filename);
				}
			} catch (IOException e) {
				currentCall = null; // shows call stopped.
				e.printStackTrace();
			} catch (Exception e) {
				currentCall = null; // shows call stopped.
				e.printStackTrace();
			}
		}
	}

	public void stopRecording() throws IllegalArgumentException,
			SecurityException, IllegalStateException, IOException {
		AppLog.i("INFO : StopRecording called");
		if (currentCall == null) {
			AppLog.i("ERROR : current call is null in stopRecording of callstateseekservice.");
			return;
		}
		if (recorder != null) {
			recorder.stopAndNotify();
			// saving recording after recorder.stop() because stop is providing
			// duration of recording file.
			// AppLog.i("INFO : "+currentCall.getContactid() + " : " +
			// currentCall.getContactnumber() + " : " + currentCall.getState() +
			// " : " + currentCall.getDate()
			// + " : " +currentCall.getDuration() + " : " +
			// currentCall.getFilepath());
			if (currentCall.getContactid() == null) {
				AppLog.i("INFO : setting contact id -1 because contact id is null");
				currentCall.setContactid(-1);
			}
			new DBManager().storeData(currentCall.getContactid(),
					currentCall.getContactnumber(), currentCall.getState(),
					currentCall.getDate(), currentCall.getDuration(),
					currentCall.getFilepath());
			currentCall = null; // shows call stopped.
		}
	}

	@Override
	public void onDestroy() {
		/*
		 * if(outgoingReciever != null) unregisterReceiver(outgoingReciever);
		 */
		super.onDestroy();
	}

}
