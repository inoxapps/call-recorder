package com.call.recorder.pro.auto;

import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.call.recorder.pro.auto.database.ContactTable;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class ContactRecordings extends Activity{
	
	public final static String ContactId = "contactid";
	private final int CONTACT_IMAGE_HEIGHT = 150;
	private final int CONTACT_IMAGE_WIDTH = 150;
	private ImageView contactimage = null;
	private TextView contactname = null;
	private ListView recordinglist = null;
	private ArrayList<ContactTable> list = null;
	private RecordingListAdapter adapter = null;
	private int contactid = -1;
	
	public final static int RESULT_LIST_EMPTY = 400;
	public final static int RESULT_LIST_NOT_EMPTY = 401;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactrecinfolayout);
		
		Intent intent = getIntent();
		if(intent == null){
			finish();
			return;
		}
		
		contactid = intent.getIntExtra(ContactId, 0);
		if(contactid < 1){
			finish();
			return;
		}
		
		contactimage = (ImageView) findViewById(R.id.ivInfoContactImage);
		contactname = (TextView) findViewById(R.id.tvInfoContactName);
		recordinglist = (ListView) findViewById(R.id.lvInfoRecordingList);
		
		try {
			Bitmap bitmap = Utilities.getContactCompressedImage(this, contactid, CONTACT_IMAGE_HEIGHT, CONTACT_IMAGE_WIDTH);
			if(bitmap != null)
				contactimage.setImageBitmap(bitmap);
			
			String name = Utilities.getContactName(getApplicationContext(), contactid);
			if(name != null)
				contactname.setText(name);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		list = new DBManager().getRecordingsFromId(contactid);
		AppLog.i("list size : "+list.size());
		adapter = new RecordingListAdapter(this, list);
		recordinglist.setAdapter(adapter);
		
		recordinglist.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int index,
					long arg3) {
				ContactTable item = list.get(index);
				if(Utilities.isFile(item.getFilepath())){
					Intent intent = new Intent(getApplicationContext(), EqualizerActivity.class);
					intent.putExtra(EqualizerActivity.CONTACT_ID, contactid);
					intent.putExtra(EqualizerActivity.PLAY_FILE_NAME, item.getFilepath());
					startActivity(intent);
				}
				else{
					Toast.makeText(getApplicationContext(), "File not present", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		recordinglist.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int index, long arg3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(ContactRecordings.this);
				builder.setCancelable(true);
		        builder.setMessage("Are you sure you want to delete this recording.");
		        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   ContactTable item = list.get(index);
		                	   String filename = new DBManager().deleteRecording(item.getContactnumber(), item.getFilepath()); 
		                	   Utilities.deleteRecording(getApplicationContext(), filename);
		                	   list.remove(index);
		                	   adapter.notifyDataSetChanged();
		                	   dialog.dismiss();
		                   }
		               });
		        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   dialog.dismiss();
		                   }
		               });
		        builder.show();
				return false;
			}
		});
	}
	
	@Override
	public void finish() {
		if (getParent() == null) {
			setResult(RESULT_CANCELED, null);
		} else {
			if(list.size() > 0)
				getParent().setResult(RESULT_LIST_NOT_EMPTY, null);
			else
				getParent().setResult(RESULT_LIST_EMPTY, null);
		}
		super.finish();
	}
	
}
