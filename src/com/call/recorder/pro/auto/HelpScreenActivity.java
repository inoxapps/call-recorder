package com.call.recorder.pro.auto;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.call.recorder.pro.auto.CustomHorizontalScrollView.OnEndAction;

public class HelpScreenActivity extends Activity {

	private RelativeLayout linearLayout;
	private CustomHorizontalScrollView horizontalScrollView;

	private ImageView finishView;
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@SuppressLint("NewApi")
	@SuppressWarnings("deprecation")
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		finishView = new ImageView(this);

		SharedPreferences statusPref = getSharedPreferences("camera", 2);
		SharedPreferences.Editor statusEditor = statusPref.edit();
		statusEditor.putBoolean("icon_created", true);
		statusEditor.commit();

		int width = getWindowManager().getDefaultDisplay().getWidth();
		horizontalScrollView = new CustomHorizontalScrollView(this, 4, width);

		if (Build.VERSION.SDK_INT >= 9)
			horizontalScrollView
					.setOverScrollMode(ScrollView.OVER_SCROLL_NEVER);
			horizontalScrollView.setHorizontalScrollBarEnabled(false);
			horizontalScrollView.setVerticalScrollBarEnabled(false);
			horizontalScrollView.setEndAction(new OnEndAction() {

			public void onLastPage() {
				HelpScreenActivity.this.finish();
			}
		});

		linearLayout = (RelativeLayout) findViewById(R.id.layer);
		linearLayout.addView(horizontalScrollView);

		LinearLayout container = new LinearLayout(this);
		container.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		// container.setHeight(height);

		ImageView helpView = new ImageView(this);

		helpView.setLayoutParams(new LayoutParams(width,
				LayoutParams.FILL_PARENT));
		helpView.setImageResource(R.drawable.one);
		helpView.setScaleType(ImageView.ScaleType.FIT_XY);
		container.addView(helpView);

		helpView = new ImageView(this);
		helpView.setLayoutParams(new LayoutParams(width,
				LayoutParams.FILL_PARENT));
		helpView.setImageResource(R.drawable.two);
		helpView.setScaleType(ImageView.ScaleType.FIT_XY);
		container.addView(helpView);

		helpView = new ImageView(this);
		helpView.setLayoutParams(new LayoutParams(width,
				LayoutParams.FILL_PARENT));
		helpView.setImageResource(R.drawable.three);
		helpView.setScaleType(ImageView.ScaleType.FIT_XY);
		container.addView(helpView);

		helpView = new ImageView(this);
		helpView.setLayoutParams(new LayoutParams(width,
				LayoutParams.FILL_PARENT));
		container.addView(helpView);

		horizontalScrollView.addView(container);

		// linearLayout.addView(finishView, params);
	}

	public int getVersion() {
		try {
			return Class.forName("android.os.Build$VERSION")
					.getField("SDK_INT").getInt(null);
		} catch (Exception ex) {
			try {
				return Integer.parseInt((String) Class
						.forName("android.os.Build$VERSION").getField("SDK")
						.get(null));
			} catch (Exception ex1) {
				return 0;
			}
		}
	}
}
