package com.call.recorder.pro.auto.alarmmanager;

import java.util.ArrayList;

import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.preference.PrefrenceManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

public class RecorderAlarm extends BroadcastReceiver{
	
	private final static String TAG = "alarm_tag";
	public final static long default_time_never = 0;
	public final static long default_time_five_mins = 1000 * 60 * 5;		// TODO remove five minute flag
	public final static long default_time_hour = 1000 * 60 * 60;
	public final static long default_time_day = default_time_hour * 24;
	public final static long default_time_five_day = default_time_day * 5;

	@Override
	public void onReceive(Context context, Intent intent){
		if(checkIfAlreadyRunning(context)){
			AppLog.i("INFO : alarm recieved");
			PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
	        wl.acquire();
	
	        // deleting all data from database.
	        DBManager manager = new DBManager();
	        ArrayList<String> list = manager.deleteData();
	        if(list == null || list.size() < 1){
	        	 AppLog.i("WARNING : recording list is null so not performing any opreation.");
	        }
	        else{
		        AppLog.i("INFO : deleting list of size : "+list.size());
		        Utilities.deleteRecording(list);
	        }
		
	        AppLog.i("INFO : releasing lock");
	        wl.release();
		}
	}
	
	public void SetAlarm(Context context){
		if(checkIfAlreadyRunning(context)){
			AppLog.i("INFO : Before setalarm checked it is already running... cancelling alarm...");
			CancelAlarm(context);
		}
		AppLog.i("INFO : setting alarm");
        AlarmManager am=(AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent(context, RecorderAlarm.class);
        PendingIntent pi = PendingIntent.getBroadcast(context, 0, i, 0);
        // if time is zero then no alarm is set.
        long time = PrefrenceManager.getDeleteRecordingTime(context);
        if(time > 0){
        	AppLog.i("Setting alarm for time : "+time);
        	am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), time, pi); // Millisec * Second * Minute
        }
    }

	public void CancelAlarm(Context context){
		AppLog.i("INFO : cancelling alarm");
        Intent intent = new Intent(context, RecorderAlarm.class);
        PendingIntent sender = PendingIntent.getBroadcast(context, 0, intent, 0);
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(sender);
    }
	
	public boolean checkIfAlreadyRunning(Context context){
		AppLog.i("INFO : checking alarm");
		Intent intent = new Intent(context, RecorderAlarm.class);
		boolean alarmstatus = (PendingIntent.getBroadcast(context, 0, 
		        intent, 
		        PendingIntent.FLAG_NO_CREATE) != null);
		return alarmstatus;
	}
	
}
