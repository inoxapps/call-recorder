package com.call.recorder.pro.auto.preference;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.call.recorder.pro.auto.R;
import com.call.recorder.pro.auto.recorder.CallRecorder;
import com.call.recorder.pro.auto.utility.Globals;

public class PrefrenceManager {

	public final static String APP_MODE = "app_mode";
	public final static int APP_MODE_ALL = 1;
	public final static int APP_MODE_SELECTED = 2;
	
	public static void setContactChanged(Context context, boolean isChange){
		SharedPreferences sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(context);
		sharedPrefs.edit().putBoolean(Globals.CONTACT_LIST_PREF_CHANGE_INDICATOR, isChange).commit();
	}
	
	public static boolean isContactListChange(Context context){
		SharedPreferences sharedPrefs = PreferenceManager
	            .getDefaultSharedPreferences(context);
		return sharedPrefs.getBoolean(Globals.CONTACT_LIST_PREF_CHANGE_INDICATOR, false);
	}

//	public static void setAppMode(Context context, int mode) {
//		SharedPreferences sharedPrefs = PreferenceManager
//				.getDefaultSharedPreferences(context);
//		sharedPrefs.edit()
//				.putInt(context.getString(R.string.set_appmode), mode).commit();
//	}
	
	public static void setDisplayFullScreen(Context context, boolean status){
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPrefs.edit().putBoolean(context.getString(R.string.fullsceenflag), status)
				.commit();
	}
	
	public static boolean isDisplayFullScreen(Context context){
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getBoolean(context.getString(R.string.fullsceenflag),
				false);
	}

	public static int getAppMode(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		boolean mode = sharedPrefs.getBoolean(context.getString(R.string.set_appmode),
				false);
		if (mode) {
			return APP_MODE_SELECTED;
		} else {
			return APP_MODE_ALL;
		}
	}

	public static void setAppStatus(Context context, boolean status) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPrefs.edit()
				.putBoolean(context.getString(R.string.set_appstatus), status)
				.commit();
	}

	public static boolean getAppStatus(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getBoolean(
				context.getString(R.string.set_appstatus), true);
	}

	public static boolean isPassActive(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getBoolean(
				context.getString(R.string.set_applypass), false);
	}

	public static boolean isShowNotification(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getBoolean(
				context.getString(R.string.set_shownotification), true);
	}

	public static boolean isSaveFileWithExt(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getBoolean(
				context.getString(R.string.set_savefilewithext), true);
	}

	public static String isOpenAppWithPassword(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		String password = sharedPrefs.getString(
				context.getString(R.string.set_openwithpassword), null);
		return password;
	}

	public static void setAppWithPasswordStatus(Context context, String password) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPrefs
				.edit()
				.putString(context.getString(R.string.set_openwithpassword),
						password).commit();
	}

	public static void setDefaultPathForRecording(Context context, String path) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPrefs.edit()
				.putString(context.getString(R.string.set_defaultpath), path)
				.commit();
	}

	public static String getDefaultPathForRecording(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getString(
				context.getString(R.string.set_defaultpath), null);
	}

	public static void setAutoCleaningOption(Context context, String path) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPrefs
				.edit()
				.putString(context.getString(R.string.set_autoCleaningOpt),
						path).commit();
	}

	public static String getAutoCleaningOption(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getString(
				context.getString(R.string.set_autoCleaningOpt), null);
	}

	public static String getRecordingSource(Context context) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedPrefs.getString(
				context.getString(R.string.set_recordingSource), String.valueOf(CallRecorder.RECORDING_SOURCE_MIC));
	}

	public static void setRecordingSource(Context context, String value) {
		SharedPreferences sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedPrefs
				.edit()
				.putString(context.getString(R.string.set_recordingSource),
						value).commit();
	}

	public static void setDeleteRecordingTime(Context context, long time) {
		SharedPreferences sharedprefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		sharedprefs.edit()
				.putLong(context.getString(R.string.set_deletealarm), time)
				.commit();
	}

	public static long getDeleteRecordingTime(Context context) {
		SharedPreferences sharedprefs = PreferenceManager
				.getDefaultSharedPreferences(context);
		return sharedprefs.getLong(context.getString(R.string.set_deletealarm),
				0);
	}
}
