package com.call.recorder.pro.auto;

import java.util.ArrayList;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.call.recorder.pro.auto.database.ContactTable;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class KnownContactFragment extends Fragment {

	private GridView contactgrid = null;
	public ArrayList<Integer> idList = null;
	ContactAdapter adapter = null;
	public int lastClickedIndex = -1;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		if (container == null) {
			return null;
		}
		final View view = (RelativeLayout) inflater.inflate(
				R.layout.knownfragmentlayout, container, false);
		contactgrid = (GridView) view.findViewById(R.id.gvContactGrid);

		DBManager manager = new DBManager();
		idList = manager.getContactIds();
		AppLog.i("list size in knownFragment : "+idList.size());
		
		adapter = new ContactAdapter(getActivity().getApplicationContext(), R.layout.contact_item, idList);
		contactgrid.setAdapter(adapter);
		
		RecordingMainActivity.FRAGMENT_IDENTIFIER = RecordingMainActivity.KNOWN_CONTACT_FRAGMENT;
		
		contactgrid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long arg3) {
				Intent intent = new Intent(getActivity().getApplicationContext(), ContactRecordings.class);
				intent.putExtra(ContactRecordings.ContactId, idList.get(position));
				lastClickedIndex = position;
				getActivity().startActivityForResult(intent ,RecordingMainActivity.CONTACT_RECORDING_INFO_RESULT);
			}
		});
		
		contactgrid.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					final int index, long arg3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setCancelable(true);
		        builder.setMessage("Are you sure you want to delete this recording.");
		        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   Integer item = idList.get(index);
		                	   ProgressDialog pdialog = new ProgressDialog(getActivity());
		                	   pdialog.setTitle("Deleting...");
		                	   pdialog.show();
		                	   ArrayList<String> list = new DBManager().deleteContactRecording(item);
		                	   Utilities.deleteRecording(list);
		                	   pdialog.dismiss();
		                	   idList.remove(index);
		                	   adapter.notifyDataSetChanged();
		                	   dialog.dismiss();
		                   }
		               });
		        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   dialog.dismiss();
		                   }
		               });
		        builder.show();
				return false;
			}
		});
		return view;
	}

	class ContactAdapter extends ArrayAdapter<Integer> {
		
		private DBManager manager = null;
		private ContactImageDisplayer imgDisplayer = null;
		private ArrayList<Integer> list = null;
		
		public ContactAdapter(Context context, int resId, ArrayList<Integer> idList) {
			super(context, resId, idList);
			manager = new DBManager();
			list = idList;
			imgDisplayer = new ContactImageDisplayer(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View row = convertView;
			ContactHolder holder = null;
			if (row == null) {
				AppLog.i("In KnownContactFragment(ContactAdapter) getview row is null. ");
				LayoutInflater inflater = (LayoutInflater) KnownContactFragment.this.getActivity()
						.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				row = inflater.inflate(R.layout.contact_item, parent, false);
				holder = new ContactHolder();
				holder.contactimage = (ImageView) row.findViewById(R.id.ivContactImage);
				holder.contactname = (TextView) row.findViewById(R.id.tvContactName);
				row.setTag(holder);
			}
			else{
				AppLog.i("In KnownContactFragment(ContactAdapter) getview row is not null. ");
				holder = (ContactHolder) row.getTag();
			}
			
			int id = list.get(position);
			String name = Utilities.getContactName(getContext(), id);
			AppLog.i("setting gridview item for id : "+id+":name"+name);
			if(name != null)
				holder.contactname.setText(name);
			imgDisplayer.DisplayImage(id, holder.contactimage);
			
			return row;
		}
	}
	
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		try {
			this.getView().setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View arg0, int arg1, KeyEvent arg2) {
					if (arg2.getKeyCode() == KeyEvent.KEYCODE_BACK) {
						getActivity().finish();
					}
					return false;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	static class ContactHolder{
		ImageView contactimage;
		TextView contactname;
	}
	
	public void deleteAllRecordings(){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		AlertDialog dialog;
		builder.setMessage("Are you sure you want to delete all recordings.");
		builder.setTitle("Delete");
		builder.setCancelable(true);
		builder.setPositiveButton("Delete",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						DBManager manager = new DBManager();
						ProgressDialog dialog2 = new ProgressDialog(
								getActivity());
						dialog2.setMessage("Deleting...");
						dialog2.show();
						ArrayList<String> list = manager.deleteKnownRecordings();
						Utilities.deleteRecording(list);
						dialog2.cancel();
						dialog.dismiss();
					}
				});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		dialog = builder.create();
		dialog.show();
	}
}
