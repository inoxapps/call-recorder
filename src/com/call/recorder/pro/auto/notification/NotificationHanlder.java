package com.call.recorder.pro.auto.notification;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.pm.PackageManager;

import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Globals;


public class NotificationHanlder {

	@SuppressWarnings("deprecation")
	public static void deliverNotification(Context context, int notificationId, String description,
			PendingIntent pIntent) {
		NotificationManager manager = ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
		
		int icon = context.getApplicationInfo().icon;
		if (icon == 0)
			icon = Globals.STUB_ICON;
		
		PackageManager pmanager = context.getPackageManager();
        String appname = pmanager.getApplicationLabel(context.getApplicationInfo()).toString();
        AppLog.i("Application name : "+appname);

		Notification notification;
		notification = new Notification(icon, description, System.currentTimeMillis());
		
		notification.flags = Notification.FLAG_AUTO_CANCEL;
		notification.setLatestEventInfo(context, appname, description, pIntent);
		
		manager.notify(notificationId, notification);
	}
}
