package com.call.recorder.pro.auto;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.call.recorder.pro.auto.database.ContactTable;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class RecordingListAdapter extends BaseAdapter {

	ArrayList<ContactTable> list;
	Activity activity;

	public RecordingListAdapter(Activity activity, ArrayList<ContactTable> list) {
		this.list = list;
		this.activity = activity;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public void deleteAll() {
		list.clear();
		notifyDataSetChanged();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		View row = convertView;
		RecordHolder holder = null;
		if (row == null) {
			LayoutInflater inflater = (LayoutInflater) activity
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			row = inflater.inflate(R.layout.recording_item, parent, false);
			holder = new RecordHolder();
			holder.tvRecName = (TextView) row.findViewById(R.id.tvRecName);
			holder.tvRecDuration = (TextView) row
					.findViewById(R.id.tvRecDuration);
			holder.tvRecDate = (TextView) row.findViewById(R.id.tvRecDate);
			holder.ivCallState = (ImageView) row.findViewById(R.id.ivCallState);
			row.setTag(holder);
		} else
			holder = (RecordHolder) row.getTag();

		ContactTable data = list.get(position);
		if (data.getState() == DBManager.CALL_TYPE_INCOMING) {
			holder.ivCallState.setImageResource(R.drawable.incoming);
		} else {
			holder.ivCallState.setImageResource(R.drawable.outgoing);
		}

		String filepath = data.getFilepath();
		String filename = filepath.substring(data.getFilepath()
				.lastIndexOf("/") + 1, filepath.length());
		String tokenizeString[] = filename.split("_");
		String contactNumber = tokenizeString[0];
		// Long filetime = Long.parseLong(tokenizeString[1]);
		String info[] = Utilities.getContactInfoFromNumber(
				activity.getApplicationContext(), contactNumber);
		if (info == null) {
			holder.tvRecName.setText(contactNumber);
		} else {
			if(info[1] != null)
				holder.tvRecName.setText(info[1]);
		}

		long date = data.getDate();
		if (date > 0) {
			AppLog.i("Setting date : " + Utilities.getDate(date));
			holder.tvRecDate.setText(Utilities.getDate(date));
		}

		long duration = data.getDuration();
		int seconds = (int) (duration / 1000) % 60;
		int minutes = (int) ((duration / (1000 * 60)) % 60);
		int hours = (int) ((duration / (1000 * 60 * 60)) % 24);
		String durationstring = null;

		AppLog.i("time " + hours + ":" + minutes + ":" + seconds);
		if (String.valueOf(hours).length() == 1) {
			durationstring = "0" + hours + ":";
		} else {
			durationstring = hours + ":";
		}

		if (String.valueOf(minutes).length() == 1) {
			durationstring = durationstring + "0" + minutes + ":";
		} else {
			durationstring = durationstring + minutes + ":";
		}

		AppLog.i("seconds length : " + String.valueOf(seconds).length());
		if (String.valueOf(seconds).length() == 1) {
			durationstring = durationstring + "0" + seconds;
		} else {
			durationstring = durationstring + seconds + "";
		}

		holder.tvRecDuration.setText(durationstring);

		return row;
	}

	static class RecordHolder {
		TextView tvRecName;
		TextView tvRecDuration;
		TextView tvRecDate;
		ImageView ivCallState;
	}

}