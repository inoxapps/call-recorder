package com.call.recorder.pro.auto;

import java.util.ArrayList;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.call.recorder.pro.auto.database.ContactTable;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class UnknownContactFragment extends Fragment{
	
	private ListView recList = null;
	private RecordingListAdapter adapter = null;
	private ArrayList<ContactTable> list;
	
	private int UNKWON_CONTACT_DEFAULT_ID = -1;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		if (container == null) {
			AppLog.i("ERROR : container is null returning...");
			return null;
		}

		final View view = (RelativeLayout) inflater.inflate(
				R.layout.unknownfragmentlayout, container, false);
		
		RecordingMainActivity.FRAGMENT_IDENTIFIER = RecordingMainActivity.UNKNOWN_CONTACT_FRAGMENT; 

		recList = (ListView) view.findViewById(R.id.lvRecList);
		list = new DBManager().getUnknownContactIds();
		AppLog.i("list size in unknownfragment : "+list.size());
		adapter = new RecordingListAdapter(getActivity(), list);
		recList.setAdapter(adapter);
		
		recList.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int index,
					long arg3) {
				ContactTable item = list.get(index);
				if(Utilities.isFile(item.getFilepath())){
					Intent intent = new Intent(getActivity().getApplicationContext(), EqualizerActivity.class);
					intent.putExtra(EqualizerActivity.CONTACT_ID, UNKWON_CONTACT_DEFAULT_ID);
					intent.putExtra(EqualizerActivity.PLAY_FILE_NAME, item.getFilepath());
					startActivity(intent);
				}
				else{
					Toast.makeText(getActivity().getApplicationContext(), "File not present", Toast.LENGTH_LONG).show();
				}
			}
		});
		
		recList.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View view,
					final int index, long arg3) {
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setCancelable(true);
		        builder.setMessage("Are you sure you want to delete this recording.");
		        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   ContactTable item = list.get(index);
		                	   String filename = new DBManager().deleteRecording(item.getContactnumber(), item.getFilepath()); 
		                	   Utilities.deleteRecording(getActivity().getApplicationContext(), filename);
		                	   list.remove(index);
		                	   adapter.notifyDataSetChanged();
		                	   dialog.dismiss();
		                   }
		               });
		        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
		                   public void onClick(DialogInterface dialog, int id) {
		                	   dialog.dismiss();
		                   }
		               });
		        builder.show();
				return false;
			}
		});
		
		return view;
	}
	
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.activity_main, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}
	
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		 switch (item.getItemId()) {
//		 case R.id.settings:
//	    	  startActivity(new Intent(getActivity().getApplicationContext(), SettingActivity.class));
//	    	  return true;
//	      case R.id.close:
////	    	  finish();
//	    	  return true;
//		 case R.id.deleteAllRec:
//	    	  AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//	    	  AlertDialog dialog ;
//	    	  builder.setMessage("Are you sure you want to delete all recordings.");
//	    	  builder.setTitle("Delete");
//	    	  builder.setCancelable(true);
//	    		builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
//	    		           public void onClick(DialogInterface dialog, int id) {
//	    		        	   DBManager manager = new DBManager();
//	    		        	   ProgressDialog dialog2 = new ProgressDialog(getActivity());
//	    		        	   dialog2.setMessage("Deleting...");
//	    		        	   dialog2.show();
//	    		        	   manager.deleteData();
//	    		        	   adapter.deleteAll();
//	    		        	   dialog2.cancel();
//	    		        	   dialog.dismiss();
//	    		           }
//	    		       });
//	    		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//	    		           public void onClick(DialogInterface dialog, int id) {
//	    		        	   dialog.dismiss();
//	    		           }
//	    		       });
//	    		dialog = builder.create();
//	    		dialog.show();
//	    	  return true;
//		 }
//		return super.onOptionsItemSelected(item);
//	}
	
	@Override
	public void onActivityCreated(Bundle bundle) {
		super.onActivityCreated(bundle);
		try {
			this.getView().setOnKeyListener(new OnKeyListener() {
				@Override
				public boolean onKey(View view, int arg1, KeyEvent arg2) {
					if (arg2.getKeyCode() == KeyEvent.KEYCODE_BACK) {
						getActivity().finish();
					}
					return false;
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
//		finally{
//			getActivity().finish();
//		}
	}
	
	public void deleteAllRecordings(){
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		AlertDialog dialog;
		builder.setMessage("Are you sure you want to delete all recordings.");
		builder.setTitle("Delete");
		builder.setCancelable(true);
		builder.setPositiveButton("Delete",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						DBManager manager = new DBManager();
						ProgressDialog dialog2 = new ProgressDialog(
								getActivity());
						dialog2.setMessage("Deleting...");
						dialog2.show();
						ArrayList<String> list = manager.deleteUnknownRecordings();
						Utilities.deleteRecording(list);
						dialog2.cancel();
						dialog.dismiss();
					}
				});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.dismiss();
					}
				});
		dialog = builder.create();
		dialog.show();
	}
}
