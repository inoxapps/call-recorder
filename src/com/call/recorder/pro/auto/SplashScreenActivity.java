package com.call.recorder.pro.auto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.Toast;

import com.call.recorder.pro.auto.service.CallStateSeekService;
import com.call.recorder.pro.auto.utility.Utilities;
import com.crittercism.app.Crittercism;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.plus.PlusClient;
import com.google.android.gms.plus.PlusOneButton;
import com.tapjoy.TapjoyConnect;
import com.tapjoy.TapjoyLog;

public class SplashScreenActivity extends Activity implements OnClickListener {

	ImageView startRecorder, moreapps, inviteFriends, shareapp, ratethisapp;
	private PlusClient mPlusClient;
	private PlusOneButton mPlusOneStandardButton;

	@Override
	protected void onStart() {
		// flurry
		FlurryAgent.onStartSession(this, "N8JBRFMH2Y74YKJTVGW6");
		mPlusClient.connect();
		super.onStart();
	}

	@Override
	protected void onResume() {
		mPlusOneStandardButton.initialize(mPlusClient,
				"https://play.google.com/store/apps/details?id="
						+ getPackageName(), 0);
		
		// com.facebook.AppEventsLogger.activateApp(this, "417284998391680");
		super.onResume();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		startService(new Intent(this, CallStateSeekService.class));

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					this.getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("MY KEY HASH:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {

		}

		// interstitial banner code
		// AppLog.i("is interstitial show : "
		// 		+ PrefrenceManager.isDisplayFullScreen(getApplicationContext()));
		// if (PrefrenceManager.isDisplayFullScreen(getApplicationContext()))
		// 	AdMediation.showInterstitial(this);
		// else
		// 	PrefrenceManager
		// 			.setDisplayFullScreen(getApplicationContext(), true);

		// Enables logging to the console.
		TapjoyLog.enableLogging(true);
		TapjoyConnect.requestTapjoyConnect(getApplicationContext(),
				"a9c576d7-5226-4804-b51a-ee1544051da7", "itq87F6sYas7GKXbeggv");

		// start app start
		// AndroidSDKProvider.initSDK(this);

		// AdMediation.cacheBackInterstitial(this);

		// Crittercism
		Crittercism.init(getApplicationContext(), "51a05b2a8b2e333982000002");

		boolean isSimReady = Utilities.isSimPresent(getApplicationContext());
		if (!isSimReady) {
			Toast.makeText(getApplicationContext(), "Sim state unknown.",
					Toast.LENGTH_LONG).show();
		}

		mPlusClient = new PlusClient.Builder(this, new ConnectionCallbacks() {
			@Override
			public void onDisconnected() {
			}

			@Override
			public void onConnected() {
			}
		}, new OnConnectionFailedListener() {
			@Override
			public void onConnectionFailed(ConnectionResult result) {
			}
		}).clearScopes().build();
		mPlusOneStandardButton = (PlusOneButton) findViewById(R.id.plus_one_button);

		startRecorder = (ImageView) findViewById(R.id.ivStart);
		startRecorder.setOnClickListener(this);

		moreapps = (ImageView) findViewById(R.id.ivMore);
		moreapps.setOnClickListener(this);

		shareapp = (ImageView) findViewById(R.id.ivShareApp);
		shareapp.setOnClickListener(this);

		ratethisapp = (ImageView) findViewById(R.id.ratethisapp);
		ratethisapp.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.ivStart:
			startActivity(new Intent(this, RecordingMainActivity.class));
			finish();
			break;
		case R.id.ivMore:
			Utilities.showMoreApps(this);
			break;
		case R.id.ivShareApp:
			Utilities.showShareApp(this);
			break;
		case R.id.ratethisapp:
			Utilities.showRateDialoge(this);
			break;
		}
	}

	@Override
	protected void onStop() {
		mPlusClient.disconnect();
		FlurryAgent.onEndSession(this);
		super.onStop();
	}

	// @Override
	// public void onBackPressed() {
	// try{
	//
	// super.onBackPressed();
	// }
	// catch(Exception e){
	// e.printStackTrace();
	// }
	// }
}
