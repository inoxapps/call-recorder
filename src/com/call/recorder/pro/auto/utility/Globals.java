package com.call.recorder.pro.auto.utility;

import java.util.Vector;

import com.call.recorder.pro.auto.contactManager.ContactDetail;


public class Globals {

	public final static int STUB_ICON = 0;
	public final static byte ASYNCH_DEFAULT = 0;
	public final static byte ASYNCH_NOT_ABLE_NOTIFY = 1;
	public final static byte ASYNCH_NOTIFYING = 2;
	public final static byte ASYNCH_COMPLETED = 3;
	public final static byte ASYNCH_COMPLETED_WITHOUT_NOTIFYING = 4;
	public static byte ASYNCH_STATUS = ASYNCH_DEFAULT;
	
	public static Vector<ContactDetail> contactDetail = new Vector<ContactDetail>();
	
	public static String CONTACT_LIST_PREF_CHANGE_INDICATOR = "contactlistchageindicator";
}
