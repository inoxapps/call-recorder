package com.call.recorder.pro.auto.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Vector;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.provider.MediaStore.Images;
import android.telephony.TelephonyManager;
import android.text.format.Formatter;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.widget.Toast;

import com.call.recorder.pro.auto.R;
import com.call.recorder.pro.auto.contactManager.ContactDetail;
import com.call.recorder.pro.auto.database.DBManager;

public class Utilities {

	private static final int RECORDER_BPP = 16;
	private static boolean isRecording = false;
	private static Thread recordingThread = null;
	private static int bufferSize = 0;
	private static final String AUDIO_RECORDER_TEMP_FILE = "record_temp.bin";

	private static final int RECORDER_SAMPLERATE = 44100;
	private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
	private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;
	
	public static void deleteRecording(Context context, String filename){
		try {
			if(filename != null && !filename.equals("")){
				AppLog.i("Deleting single file : "+filename);
				File delFile = new File(filename);
				if(delFile.isFile()){
					delFile.delete();
				}
			}
		} catch (Exception e) {
			Toast.makeText(context, "File can not be deleted.", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}
	}
	
	public static void deleteRecording(ArrayList<String> list){
		if(list == null || list.size() <= 0)
			return;
		Iterator<String> listIterator = list.iterator();
		while(listIterator.hasNext()){
			String path = listIterator.next();
			AppLog.i("Deleting path : "+path);
			File delFile = new File(path);
			if(delFile.isFile()){
				delFile.delete();
			}
		}
	}
	
	public static synchronized Vector<ContactDetail> getContactsFromString(
			String containString, Vector<ContactDetail> list,
			Vector<ContactDetail> filteredList) {

		containString = containString.toLowerCase();

		if (filteredList == null) {
			Log.v("check", "filtered list is null");
			filteredList = new Vector<ContactDetail>();
			for (ContactDetail ct : list) {
				Log.v("check", "checking name : " + ct.getName()
						+ " and result : "
						+ ct.getName().toLowerCase().startsWith(containString));
				if (ct.getName().toLowerCase().startsWith(containString)
						|| (ct.getNumberObj() != null && ct.getNumberObj()
								.getNumber().startsWith(containString))) {
					filteredList.add(ct);
				}
			}
		} else {
			Log.v("check", "filtered list is not null");
			@SuppressWarnings("unchecked")
			Vector<ContactDetail> tempList = (Vector<ContactDetail>) filteredList
					.clone();
			filteredList.clear();
			for (ContactDetail ct : tempList) {
				if (ct.getName().toLowerCase().startsWith(containString)
						|| (ct.getNumberObj() != null && ct.getNumberObj()
								.getNumber().startsWith(containString))) {
					filteredList.add(ct);
				}
			}
		}
		return filteredList;
	}

	public static boolean checkValidNumber(String number) {
		if (number == null || number.startsWith("*") || number.startsWith("#")) {
			return false;
		}
		return true;
	}

	public static Bitmap getContactCompressedImage(Activity activity, int id, int displayheight, int displaywidth)
			throws IOException {
		try {
			InputStream inputStream = getContactPhotoInputStream(
					activity.getApplicationContext(), id);
			if (inputStream == null)
				return null;
			Display currentDisplay = activity.getWindowManager()
					.getDefaultDisplay();

			DisplayMetrics metrix = new DisplayMetrics();
			currentDisplay.getMetrics(metrix);
//			int dw = metrix.widthPixels;// currentDisplay.getWidth();
//			int dh = metrix.heightPixels;// currentDisplay.getHeight();
			int dw = displaywidth;
			int dh = displayheight;

			BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
			bmpFactoryOptions.inJustDecodeBounds = true;
			Bitmap bitmap = BitmapFactory.decodeStream(inputStream, null,
					bmpFactoryOptions);

			int heightRatio = (int) Math.ceil(bmpFactoryOptions.outHeight
					/ (float) dh);
			int widthRatio = (int) Math.ceil(bmpFactoryOptions.outWidth
					/ (float) dw);

			if (heightRatio > 1 && widthRatio > 1) {
				if (heightRatio > widthRatio) {
					bmpFactoryOptions.inSampleSize = heightRatio;
				} else {
					bmpFactoryOptions.inSampleSize = widthRatio;
				}
			}

			bmpFactoryOptions.inJustDecodeBounds = false;
			inputStream.close();
			inputStream = getContactPhotoInputStream(
					activity.getApplicationContext(), id);
			if (inputStream == null)
				return null;
			bitmap = BitmapFactory.decodeStream(inputStream, null,
					bmpFactoryOptions);
			inputStream.close();
			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public Bitmap rotate(Bitmap b, int degrees) {
		if (degrees != 0 && b != null) {
			Matrix m = new Matrix();
			m.setRotate(degrees, (float) b.getWidth() / 2,
					(float) b.getHeight() / 2);
			try {
				Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
						b.getHeight(), m, true);
				if (b != b2) {
					b.recycle();
					b = b2;
				}
			} catch (OutOfMemoryError ex) {
				throw ex;
			}
		}
		return b;
	}

	public float checckRotationForImage(Context context, Uri uri) {
		if (uri.getScheme().equals("content")) {
			String[] projection = { Images.ImageColumns.ORIENTATION };
			Cursor c = context.getContentResolver().query(uri, projection,
					null, null, null);
			if (c.moveToFirst()) {
				return c.getInt(0);
			}
		} else if (uri.getScheme().equals("file")) {
			try {
				ExifInterface exif = new ExifInterface(uri.getPath());
				int rotation = (int) exifOrientationToDegrees(exif
						.getAttributeInt(ExifInterface.TAG_ORIENTATION,
								ExifInterface.ORIENTATION_NORMAL));
				return rotation;
			} catch (IOException e) {
			}
		}
		return 0f;
	}

	public float exifOrientationToDegrees(int exifOrientation) {
		if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) {
			return 90;
		} else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {
			return 180;
		} else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {
			return 270;
		}
		return 0;
	}

	public static boolean isFile(String path) {
		File file = new File(path);
		return file.isFile();
	}

	public static String getDate(long callDate) {
		if (callDate > 0) {
			String dateString = new SimpleDateFormat("dd/MM/yyyy")
					.format(new Date(callDate));
			return dateString;
		}
		return null;
	}

	public static String getTodayDate() {
		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String formattedDate = df.format(c.getTime());
		return formattedDate;
	}

	// return information array having contactid,contactname and
	// contactinformation in respective manner.
	public static String[] getContactInfoFromNumber(Context context,
			String number) {

		String[] PEOPLE_PROJECTION = new String[] {
				ContactsContract.PhoneLookup._ID,
				ContactsContract.PhoneLookup.DISPLAY_NAME };
		Uri contactUri = Uri.withAppendedPath(
				ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
				Uri.encode(number));
		try {
			Cursor phonescur = context.getContentResolver().query(contactUri,
					null, null, null, null);
			if (phonescur == null) {
				return null;
			}
			AppLog.i("cursor check : " + phonescur.getColumnCount());
			String info[] = new String[3];
			if (phonescur.moveToFirst()) {
				info[0] = phonescur.getString(phonescur
						.getColumnIndex(ContactsContract.Contacts._ID));
				info[1] = phonescur
						.getString(phonescur
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

				if (Integer
						.parseInt(phonescur.getString(phonescur
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					AppLog.i("Getting contact number for : " + info[0]);
					Cursor pCur = context.getContentResolver().query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { info[0] }, null);
					if (pCur == null) {
						AppLog.i("WARNING: cursor is null means contact has no number");
						info[2] = number;
					} else {
						while (pCur.moveToNext()) {
							String MainNumber = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

							info[2] = MainNumber;
							break;
						}
						pCur.close();
					}
				}
				phonescur.close();
				return info;
			}
			phonescur.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
//	public static String getContactName(Context context, int id) {
//		 
//        String contactName = null;
//        
////        Uri uriContact = Uri.withAppendedPath(
////                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, String.valueOf(id));
//        
//        Uri uriContact = Uri.withAppendedPath(
//				ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
//				Uri.encode(String.valueOf(id)));
// 
//        // querying contact data store
//        Cursor cursor = context.getContentResolver().query(uriContact, null, null, null, null);
// 
//        if (cursor.moveToFirst()) {
// 
//            // DISPLAY_NAME = The display name for the contact.
//            // HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.
// 
//            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//        }
// 
//        cursor.close();
// 
//        AppLog.i("Contact Name: " + contactName);
//        return contactName;
// 
//    }

	public static String getContactName(Context context, int id) {
		
//		String[] PEOPLE_PROJECTION = new String[] {
//				ContactsContract.PhoneLookup._ID,
//				ContactsContract.PhoneLookup.DISPLAY_NAME };
//		Uri contactUri = Uri.withAppendedPath(
//				ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
//				Uri.encode(number));
//		try {
//			Cursor phonescur = context.getContentResolver().query(contactUri,
//					null, null, null, null);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}

		String[] PEOPLE_PROJECTION = new String[] { ContactsContract.PhoneLookup.DISPLAY_NAME, };

		try {
			Cursor phonescur = context.getContentResolver().query(
					ContactsContract.Contacts.CONTENT_URI, PEOPLE_PROJECTION,
					ContactsContract.Contacts._ID + "=" + id, null, null);
			if (phonescur == null) {
				return null;
			}
			AppLog.i("cursor check : " + phonescur.getColumnCount());
			String name = null;
			if (phonescur.moveToFirst()) {
				name = phonescur
						.getString(phonescur
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

			}
			phonescur.close();
			return name;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static synchronized Bitmap getContactPhoto(Context context, int id)
			throws IOException {
		if (id > 0) {
			Bitmap photo = null;
			InputStream inputStream = ContactsContract.Contacts
					.openContactPhotoInputStream(context.getContentResolver(),
							ContentUris.withAppendedId(
									ContactsContract.Contacts.CONTENT_URI,
									Long.valueOf(id)));
			if (inputStream != null) {
				photo = BitmapFactory.decodeStream(inputStream);
				inputStream.close();
			}
			return photo;
		}
		return null;
	}

	public static synchronized InputStream getContactPhotoInputStream(
			Context context, int id) throws IOException {
		if (id > 0) {
			InputStream inputStream = ContactsContract.Contacts
					.openContactPhotoInputStream(context.getContentResolver(),
							ContentUris.withAppendedId(
									ContactsContract.Contacts.CONTENT_URI,
									Long.valueOf(id)));
			if (inputStream != null) {
				return inputStream;
			}
			return null;
		}
		return null;
	}

	public static void getKeyHash(Context context) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(
					context.getPackageName(), PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				Log.e("MY KEY HASH:",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (NameNotFoundException e) {

		} catch (NoSuchAlgorithmException e) {
		}
	}

	public static Date getDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = sdf.format(new java.util.Date());
		Date date = Date.valueOf(strDate);
		return date;
	}

	public static long getDateInMilli() {
		return System.currentTimeMillis();
	}

	public static long getTime() {
		return System.currentTimeMillis();
	}

	public static int getTotalRecordings(Context context) {
		DBManager manager = new DBManager();
		return manager.getRecordingCount();
	}

	public static String getSDPath(Context context) throws IOException {
		String path = null;
		if (Environment.getExternalStorageState().equals("mounted")) {
			path = Environment.getExternalStorageDirectory().getAbsolutePath()
					+ "/CallRecorder/";
		} else {
			Toast.makeText(context, "Memory card not inserted.",
					Toast.LENGTH_LONG).show();
			throw new IOException("Memory card not inserted.");
		}
		return path;
	}

	public static boolean isSimPresent(Context context) {
		TelephonyManager telephoneMgr = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		if (telephoneMgr.getSimState() == TelephonyManager.SIM_STATE_READY) {
			return true;
		}
		return false;
	}

	public boolean isMemoryAvailable(File path, Context context,
			String checkMemorysize) {
		if (path.getPath().contains("data/data")) {
			path = Environment.getDataDirectory();
		}
		/* Here checkMemorysize unit is assuming bytes. */
		if (path != null && context != null && checkMemorysize != null) {

			long availableBlocks = 0;
			long blockSize = 0;
			try {
				StatFs stat = new StatFs(path.getPath());
				blockSize = stat.getBlockSize();
				availableBlocks = stat.getAvailableBlocks();
			} catch (Exception e) {
				AppLog.i(e.getMessage());
				e.printStackTrace();
			}
			if (availableBlocks == 0) {
				return false;
			}
			String availablememory = Formatter.formatFileSize(context,
					availableBlocks * blockSize);
			if (availablememory.contains("BYTES")) {
				try {
					int availablemem = new Integer(availablememory.replace(
							"BYTES", ""));
					if (availablemem > new Integer(checkMemorysize)) {
						return true;
					}
					return false;
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				return true;
			}
		}
		return false;
	}

	public static String getBasePath(Context context) {
		if (Environment.getExternalStorageState().equals("mounted")) {
			String path = Environment.getExternalStorageDirectory()
					.getAbsolutePath();
			return path + "/";
		} else {
			Toast.makeText(context, "Memory card not inserted.",
					Toast.LENGTH_LONG).show();
			AppLog.i("WARNING : Utilities find no memory card returning null.");
			return null;
		}
	}

	public static boolean startRecording(Context context,
			final AudioRecord recorder, String filepath)
			throws IllegalStateException, IOException {
		if (filepath == null)
			return false;
		filepath = filepath + "/" + AUDIO_RECORDER_TEMP_FILE;
		File directory = new File(filepath).getParentFile();
		if (!directory.exists() && !directory.mkdirs()) {
			return false;
		}

		if (recorder != null) {
			recorder.startRecording();
			isRecording = true;
			recordingThread = new Thread(new Runnable() {

				@Override
				public void run() {
					writeAudioDataToFile(recorder);
				}
			}, "AudioRecorder Thread");

			recordingThread.start();

		}
		return true;
	}

	private static void writeAudioDataToFile(AudioRecord recorder) {
		byte data[] = new byte[bufferSize];
		String filename = getTempFilename();
		FileOutputStream os = null;

		try {
			os = new FileOutputStream(filename);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int read = 0;

		if (null != os) {
			while (isRecording) {
				read = recorder.read(data, 0, bufferSize);

				if (AudioRecord.ERROR_INVALID_OPERATION != read) {
					try {
						os.write(data);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}

			try {
				os.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void deleteTempFile() {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File tempFile = new File(filepath, AUDIO_RECORDER_TEMP_FILE);
		tempFile.delete();
	}

	private static String getTempFilename() {
		String filepath = Environment.getExternalStorageDirectory().getPath();
		File tempFile = new File(filepath, AUDIO_RECORDER_TEMP_FILE);
		File file = (tempFile).getParentFile();
		if (!file.exists() && !file.mkdirs()) {
			Log.i("Utilities", "Path to file could not be created.");
		}

		/*
		 * if(tempFile.exists()) tempFile.delete();
		 */
		return (Environment.getExternalStorageDirectory().getPath() + "/" + AUDIO_RECORDER_TEMP_FILE);
	}

	public static AudioRecord getAudioRecordInstance() {
		bufferSize = AudioRecord.getMinBufferSize(RECORDER_SAMPLERATE,
				RECORDER_CHANNELS, RECORDER_AUDIO_ENCODING);
		AudioRecord recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
				RECORDER_SAMPLERATE, RECORDER_CHANNELS,
				RECORDER_AUDIO_ENCODING, bufferSize);
		return recorder;
	}

	public static void stopAndSaveRecording(AudioRecord recorder,
			String origFilename) {
		if (null != recorder) {
			isRecording = false;
			try {
				recorder.stop();
				recorder.release();
			} catch (Exception e) {
				e.printStackTrace();
			}

			recorder = null;
			recordingThread = null;
		}

		Log.i("Utilities", "saving recording from : " + getTempFilename()
				+ " : " + origFilename);
		copyWaveFile(getTempFilename(), origFilename);
		deleteTempFile();
	}

	private static void copyWaveFile(String inFilename, String outFilename) {
		FileInputStream in = null;
		FileOutputStream out = null;
		long totalAudioLen = 0;
		long totalDataLen = totalAudioLen + 36;
		long longSampleRate = RECORDER_SAMPLERATE;
		int channels = 1;
		long byteRate = RECORDER_BPP * RECORDER_SAMPLERATE * channels / 8;

		byte[] data = new byte[bufferSize];

		try {
			in = new FileInputStream(inFilename);
			out = new FileOutputStream(outFilename);
			totalAudioLen = in.getChannel().size();
			totalDataLen = totalAudioLen + 36;

			WriteWaveFileHeader(out, totalAudioLen, totalDataLen,
					longSampleRate, channels, byteRate);

			while (in.read(data) != -1) {
				out.write(data);
			}

			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void WriteWaveFileHeader(FileOutputStream out,
			long totalAudioLen, long totalDataLen, long longSampleRate,
			int channels, long byteRate) throws IOException {

		byte[] header = new byte[44];

		header[0] = 'R'; // RIFF/WAVE header
		header[1] = 'I';
		header[2] = 'F';
		header[3] = 'F';
		header[4] = (byte) (totalDataLen & 0xff);
		header[5] = (byte) ((totalDataLen >> 8) & 0xff);
		header[6] = (byte) ((totalDataLen >> 16) & 0xff);
		header[7] = (byte) ((totalDataLen >> 24) & 0xff);
		header[8] = 'W';
		header[9] = 'A';
		header[10] = 'V';
		header[11] = 'E';
		header[12] = 'f'; // 'fmt ' chunk
		header[13] = 'm';
		header[14] = 't';
		header[15] = ' ';
		header[16] = 16; // 4 bytes: size of 'fmt ' chunk
		header[17] = 0;
		header[18] = 0;
		header[19] = 0;
		header[20] = 1; // format = 1
		header[21] = 0;
		header[22] = (byte) channels;
		header[23] = 0;
		header[24] = (byte) (longSampleRate & 0xff);
		header[25] = (byte) ((longSampleRate >> 8) & 0xff);
		header[26] = (byte) ((longSampleRate >> 16) & 0xff);
		header[27] = (byte) ((longSampleRate >> 24) & 0xff);
		header[28] = (byte) (byteRate & 0xff);
		header[29] = (byte) ((byteRate >> 8) & 0xff);
		header[30] = (byte) ((byteRate >> 16) & 0xff);
		header[31] = (byte) ((byteRate >> 24) & 0xff);
		header[32] = (byte) (2 * 16 / 8); // block align
		header[33] = 0;
		header[34] = RECORDER_BPP; // bits per sample
		header[35] = 0;
		header[36] = 'd';
		header[37] = 'a';
		header[38] = 't';
		header[39] = 'a';
		header[40] = (byte) (totalAudioLen & 0xff);
		header[41] = (byte) ((totalAudioLen >> 8) & 0xff);
		header[42] = (byte) ((totalAudioLen >> 16) & 0xff);
		header[43] = (byte) ((totalAudioLen >> 24) & 0xff);

		out.write(header, 0, 44);
	}
	
	public static void showMoreApps(Activity context) {
		context.startActivity(new Intent(
				Intent.ACTION_VIEW,
				Uri.parse("https://play.google.com/store/apps/developer?id=Xingaad")));
	}
	
	public static void showShareApp(Activity activity){
		Intent share = new Intent(Intent.ACTION_SEND);
		   share.setType("text/plain");
		   String myString = "I am using this awesome "
		     + activity.getApplicationContext().getString(R.string.app_name) + " application on Android.";
		   myString += "\n You can download this app from Google Play: https://play.google.com/store/apps/details?id=";
		   myString += activity.getApplicationContext().getPackageName();
		   myString = getCaptionShareApp(activity.getApplicationContext());
		   share.putExtra(android.content.Intent.EXTRA_SUBJECT, "Awesome "
		     + activity.getApplicationContext().getString(R.string.app_name));
		   share.putExtra(android.content.Intent.EXTRA_TITLE, "Awesome "
		     + activity.getApplicationContext().getString(R.string.app_name));
		   share.putExtra(android.content.Intent.EXTRA_TEXT, myString);
		   share.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		   activity.startActivity(Intent.createChooser(share, "Choose"));
	}
	
	private static String getCaptionShareApp(Context context) {
		  String string = "";
		  string += "Hey there,\n\nI found Call recorder app on the Play Store.";
		  string += "\nYou can record calls for contacts.";
		  string += "\nDownload it for free on: \nhttps://play.google.com/store/apps/details?id=%s";
		  string = String.format(string, context.getPackageName());
		  return string;
		 }
	
	@SuppressLint("NewApi")
	public static void showRateDialoge(final Activity activity) {
		try {
			AppLog.i("Showing rate dialog.");
			AlertDialog.Builder builder;
			if (Build.VERSION.SDK_INT >= 11) {
				builder = new AlertDialog.Builder(activity,
						android.R.style.Theme_Holo_Light_Panel);
			} else
				builder = new AlertDialog.Builder(activity);
			builder.setIcon(R.drawable.icon);
			builder.setTitle(R.string.app_name);
			builder.setMessage(
					"We are constantly working to improve this app for you. Please help us by giving your valuable feedback.\n\nHow about sharing your experience?")
					.setCancelable(false)
					.setNegativeButton("Later",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									dialog.cancel();
									activity.finish();
//									activity.showStartAppExit();
								}
							})
					.setNeutralButton("Needs Work",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									if(isInternetConnected(activity.getApplicationContext())){
										SharedPreferences.Editor editor = PreferenceManager
												.getDefaultSharedPreferences(activity.getApplicationContext()).edit();
										editor.putBoolean("rate_app", false);
										editor.commit();
										Intent share = new Intent(
												Intent.ACTION_SEND);
										share.setType("message/rfc822");
										share.putExtra(
												android.content.Intent.EXTRA_EMAIL,
												new String[] { "appfeedbacks@gmail.com" });
										share.putExtra(
												android.content.Intent.EXTRA_SUBJECT,
												"Feedback for Talking Caller ID");
										share.putExtra(
												android.content.Intent.EXTRA_TEXT,
												"");
										activity.startActivity(Intent
												.createChooser(share, "Choose"));
//										SharedPreferences.Editor editor2 = activity.sharedPrefs
//												.edit();
//										editor2.putInt("rateapp", 3);
//										editor2.commit();
										// rateUtills.setRatingStatusDone();
									}
									else{
										Toast.makeText(activity.getApplicationContext(), "Internet is not connected", Toast.LENGTH_LONG).show();
									}
									dialog.cancel();
									activity.finish();
								}
							})
					.setPositiveButton("Love it!",
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int id) {
									if(isInternetConnected(activity.getApplicationContext())){
									SharedPreferences.Editor editor = PreferenceManager
											.getDefaultSharedPreferences(activity.getApplicationContext()).edit();
									editor.putBoolean("rate_app", false);
									editor.commit();
									activity.startActivity(new Intent(
											Intent.ACTION_VIEW,
											Uri.parse("market://details?id="
													+ activity.getPackageName())));
									}
									else{
										Toast.makeText(activity.getApplicationContext(), "Internet is not connected", Toast.LENGTH_LONG).show();
									}
									dialog.cancel();
								}

							});
			AlertDialog alert = builder.create();
			alert.show();
		} catch (Exception e) {
		}
	}
	
	public static boolean isInternetConnected(Context ctx) {
		final ConnectivityManager conMgr = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		final NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
		try {
			if (activeNetwork != null && activeNetwork.isAvailable() && activeNetwork.isConnected()) {
				AppLog.i( "Internet is available");
				return true;
			}
		} catch (SecurityException e) {
			AppLog.i("Error checking internet connection e:%s" + e.getMessage());
		}
		AppLog.i("Internet not available");
		return false;
	}
}
