package com.call.recorder.pro.auto.utility;

import android.util.Log;

public class AppLog {
	private final static String TAG = "callrecorder";
	private final static Boolean LOG_ENABLE = true;

	public static void i(String message) {
		if (LOG_ENABLE) {
			Log.i(TAG, message);
		}
	}

	public static void e(String message) {
		if (LOG_ENABLE) {
			Log.e(TAG, message);
		}
	}

	public static void custom(String TAG, String message) {
		if (LOG_ENABLE) {
			Log.i(TAG, message);
		}
	}

	public static void info(String message) {

		Log.i(TAG + "info", message);

	}

	public static void error(String message) {

		Log.e(TAG + "error", message);

	}

}
