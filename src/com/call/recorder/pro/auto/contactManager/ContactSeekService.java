package com.call.recorder.pro.auto.contactManager;

import android.app.Service;
import android.content.Intent;
import android.database.ContentObserver;
import android.os.IBinder;
import android.provider.ContactsContract;

import com.call.recorder.pro.auto.utility.AppLog;

public class ContactSeekService extends Service{
	
	ContactContentObserver contactDataObserver = new ContactContentObserver();

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onCreate(){
		AppLog.i("Registering content observer");
        getApplicationContext().getContentResolver().registerContentObserver( ContactsContract.Contacts.CONTENT_URI, false, contactDataObserver );
	}
	
	private class ContactContentObserver extends ContentObserver {

        public ContactContentObserver() {
            super(null);
        }
        
        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
            AppLog.i("WARNING : Contact list changed setting prefrence.");
            com.call.recorder.pro.auto.preference.PrefrenceManager.setContactChanged(getApplicationContext(), true);
//            if( Globals.tempcontactDetail.size() <= 0 ){
//            	Globals.tempcontactDetail = Utilities.loadContacts( getApplicationContext() );
//            }
        }
    }
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		AppLog.i("Unregistering content observer");
		getApplicationContext().getContentResolver().unregisterContentObserver( contactDataObserver );
	}

}
