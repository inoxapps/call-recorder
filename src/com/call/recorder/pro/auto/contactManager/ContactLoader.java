package com.call.recorder.pro.auto.contactManager;

import java.net.URL;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Globals;

public class ContactLoader extends AsyncTask<URL, Integer, Long> {
	public ProgressDialog progress = null;
	private Context context;
	private ContactsArrayAdapter adapter = null;
	private ListView listview = null;
	private Cursor cur = null;

	public ContactLoader(Context context) {
		this.context = context;
	}

	public ContactLoader(Activity activity, ContactsArrayAdapter adapter,
			ListView listview) {
		this.progress = new ProgressDialog(activity);
		this.adapter = adapter;
		this.listview = listview;
		this.context = activity.getApplicationContext();

	}

	public void notifyAdapter(ContactsArrayAdapter adapter,
			ListView listview, TextView textview) {
		this.adapter = adapter;
		this.listview = listview;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if (progress != null) {
			progress.setMessage("loading Contacts...");
			progress.show();
			progress.setCancelable(false);
		}
	}

	@Override
	protected Long doInBackground(URL... arg0) {
		String strOrder = ContactsContract.Contacts.DISPLAY_NAME
				+ " COLLATE NOCASE ASC";
		String selection = ContactsContract.Contacts.HAS_PHONE_NUMBER + ">0";
		cur = context.getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null,
				selection, null, strOrder);

		String id = null, name = null, email = null;
		if (cur != null && cur.getCount() > 0) {
			int sizeForProg = cur.getCount() / 2;
			while (cur.moveToNext()) {
				if (progress != null && progress.isShowing()) {
					if (cur.getCount() > 25 && cur.getPosition() > 25) {
						progress.cancel();
						progress = null;
					} else if (cur.getCount() <= 25) {
						progress.cancel();
					}
				}
				final ContactDetail detail = new ContactDetail();
				email = "";
				name = "";
				id = cur.getString(cur
						.getColumnIndex(ContactsContract.Contacts._ID));
				name = cur
						.getString(cur
								.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				detail.setContactid(id);
				detail.setName(name);

				if (Integer
						.parseInt(cur.getString(cur
								.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
					ContactDetail.ContactNumber contactnumber = detail.new ContactNumber();
					Cursor pCur = context.getContentResolver().query(
							ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
							null,
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID
									+ " = ?", new String[] { id }, null);
					try {
						while (pCur.moveToNext()) {
							String phonetype = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
							String MainNumber = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							if (MainNumber != null) {
								int type = Integer.valueOf(phonetype);
								if (type == ContactDetail.ContactNumber.TYPE_HOME) {
									contactnumber
											.setType(ContactDetail.ContactNumber.TYPE_HOME);
								} else if (type == ContactDetail.ContactNumber.TYPE_MOBILE) {
									contactnumber
											.setType(ContactDetail.ContactNumber.TYPE_MOBILE);
								} else {
									contactnumber
											.setType(ContactDetail.ContactNumber.TYPE_WORK);
								}
								contactnumber.setNumber(MainNumber);
							}
							detail.setNumberObj(contactnumber);
							if (adapter != null) {
								if (listview.getWindowVisibility() == View.VISIBLE) {
									listview.post(new Runnable() {
										@Override
										public void run() {
											Globals.contactDetail
													.add(detail);
											Globals.ASYNCH_STATUS = Globals.ASYNCH_NOTIFYING;
											adapter.notifyDataSetChanged();
										}
									});
								} else {
									Globals.ASYNCH_STATUS = Globals.ASYNCH_NOT_ABLE_NOTIFY;
									Globals.contactDetail.add(detail);
								}
							} else {
								Globals.contactDetail.add(detail);
							}
						}
						pCur.close();
					} catch (Exception e) {

					}
				}
			}
			cur.close();
		}
		else{
			if (progress != null && progress.isShowing()) {
				progress.dismiss();
			}
		}
		return 1l;
	}

	@Override
	protected void onPostExecute(Long result) {
		super.onPostExecute(result);
		 AppLog.i("changing status to ASYNCH_COMPLETED.");
		if (Globals.ASYNCH_STATUS == Globals.ASYNCH_NOT_ABLE_NOTIFY) {
			Globals.ASYNCH_STATUS = Globals.ASYNCH_COMPLETED_WITHOUT_NOTIFYING;
		} else {
			Globals.ASYNCH_STATUS = Globals.ASYNCH_COMPLETED;
		}
	}

	@Override
	protected void onCancelled() {
		super.onCancelled();
	}

}
