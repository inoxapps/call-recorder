package com.call.recorder.pro.auto.contactManager;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

import com.call.recorder.pro.auto.R;
import com.call.recorder.pro.auto.utility.AppLog;

public class ImageLoader {
	private int stub_id;
	private MemoryCache memoryCache = new MemoryCache();
	private Map<Object, ImageView> imageViews = Collections
			.synchronizedMap(new LinkedHashMap<Object, ImageView>());
	private ExecutorService executorService;
	private Handler handler = new Handler();// handler to display images in UI
											// thread
	private byte FAVOURITE_GRID_ACTIVITY = 1;
	private Context context;

	public ImageLoader(Context context, byte activityIdentifier) {
		this.context = context;
		executorService = Executors.newFixedThreadPool(20);
			stub_id = R.drawable.pic_medium;
	}

	public void DisplayImage(Object detail, ImageView imageView) {
		if (imageViews.containsKey(detail)) {
			imageViews.put(detail, imageView);
		} else {
			imageViews.put(detail, imageView);
		}
		Bitmap bitmap = memoryCache.get(detail);
		if (bitmap != null) {
			imageView.setImageBitmap(bitmap);
		} else {
			PhotoToLoad p = new PhotoToLoad(detail, imageView);
			executorService.submit(new PhotosLoader(p, ImageLoader.this.context));
		}
	}

	private Bitmap getBitmap(Object detail, Context context) throws IOException {
		Bitmap b = null;
		if (detail instanceof ContactDetail) {
			AppLog.i("check detail obj : "+detail);
			b = ((ContactDetail) detail).getContactPhoto(context);
		}

		if (b != null) {
			return b;
		}
		return null;
	}

	// Task for the queue
	private class PhotoToLoad {
		public Object detail;
		public ImageView imageView;

		public PhotoToLoad(Object d, ImageView i) {
			detail = d;
			imageView = i;
		}
	}

	class PhotosLoader implements Runnable {
		PhotoToLoad photoToLoad;

		PhotosLoader(PhotoToLoad photoToLoad, Context context) {
			this.photoToLoad = photoToLoad;
			ImageLoader.this.context = context;
		}

		@Override
		public void run() {
			try {
				if (imageViewReused(photoToLoad)) {
					return;
				}

				Bitmap bmp = getBitmap(photoToLoad.detail, ImageLoader.this.context);
				if (bmp == null) {
					bmp = BitmapFactory.decodeResource(
							ImageLoader.this.context.getResources(), stub_id);
				}
				memoryCache.put(photoToLoad.detail, bmp);

				BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
				handler.post(bd);
			} catch (Throwable th) {
				th.printStackTrace();
			}
		}
	}

	boolean imageViewReused(PhotoToLoad photoToLoad) {
		ImageView imageview = imageViews.get(photoToLoad.detail);
		if (imageview == null)
			return true;
		return false;
	}

	// Used to display bitmap in the UI thread
	class BitmapDisplayer implements Runnable {
		Bitmap bitmap;
		PhotoToLoad photoToLoad;

		public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
			bitmap = b;
			photoToLoad = p;
		}

		public void run() {
			if (bitmap != null) {
				//InoxLog.v(TAG, "image contact photo is setting");
				photoToLoad.imageView.setImageBitmap(bitmap);
			} else {
				photoToLoad.imageView.setImageResource(stub_id);
			}
		}
	}

	public void clearCache() {
		memoryCache.clear();
	}

}
