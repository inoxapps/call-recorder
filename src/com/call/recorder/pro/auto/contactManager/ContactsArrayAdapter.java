package com.call.recorder.pro.auto.contactManager;

import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.call.recorder.pro.auto.R;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.utility.AppLog;

public class ContactsArrayAdapter extends BaseAdapter implements SectionIndexer {

	private Context context;
	private byte PROCESS_WHOLE_LIST = 1;
	private byte NOT_PROCESS_WHOLE_LIST = 2;
	private int layoutResourceId;
	private LayoutInflater inflater = null;
	public List<ContactDetail> data = null;
	private ImageLoader imageLoader = null;
	private HashMap<String, Integer> alphaIndexer = new HashMap<String, Integer>();;
	private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private ContactAdapterContentObserver observer;
	private ContactsArrayAdapter adapter;
	private DBManager manager;

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
		super.registerDataSetObserver(observer);
	}

	public List<ContactDetail> getData() {
		return data;
	}

	public ContactsArrayAdapter(Context context, int layoutResourceId,
			List<ContactDetail> data) {
		this.context = context;
		this.layoutResourceId = layoutResourceId;
		adapter = this;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imageLoader = new ImageLoader(context, (byte) 2);
		this.data = data;
		if (data.size() != 0) {
			fillAlphaIndexer(data, PROCESS_WHOLE_LIST);
		}
		observer = new ContactAdapterContentObserver();
		registerDataSetObserver(observer);
		
		manager = new DBManager();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		if (data != null) {
			ContactHolder holder = null;
			if (row == null) {
				holder = new ContactHolder();
				row = inflater.inflate(layoutResourceId, parent, false);
				holder.imgContactphoto = (ImageView) row
						.findViewById(R.id.ImgContactimg);
				holder.txtDisplayName = (TextView) row
						.findViewById(R.id.txtContactName);
				holder.txtHiddenHeaderSegment = (TextView) row
						.findViewById(R.id.headerSegment);
				holder.imgCheckBox = (ImageView) row
						.findViewById(R.id.ivCheckbox);
				row.setTag(holder);
			} else {
				holder = (ContactHolder) row.getTag();
			}

			ContactDetail contact = data.get(position);
			imageLoader.DisplayImage(contact, holder.imgContactphoto);

			String name = contact.getName();
			name = name.toLowerCase();
			char c = name.charAt(0);
			int asciicode = (int) c;
			String s;
			if (!(asciicode >= 97 && asciicode <= 122)) {
				s = "#";
			} else {
				s = data.get(position).getName().substring(0, 1).toUpperCase();
			}

			if (!alphaIndexer.containsKey(s)) {
				alphaIndexer.put(s, position);
				holder.txtHiddenHeaderSegment.setVisibility(View.VISIBLE);
				holder.txtHiddenHeaderSegment.setText(s);
			} else {
				if (alphaIndexer.get(s) == position) {
					holder.txtHiddenHeaderSegment.setVisibility(View.VISIBLE);
					holder.txtHiddenHeaderSegment.setText(s);
				} else {
					holder.txtHiddenHeaderSegment.setVisibility(View.GONE);
				}
			}
			
			boolean isselected = manager.isContactSelected(context, contact.getContactid());
			AppLog.i("contact "+contact.getName()+" status :"+isselected);
			if(isselected){
				holder.imgCheckBox.setTag(true);
				holder.imgCheckBox.setImageResource(R.drawable.checkbox_selected);
			}
			else{
				holder.imgCheckBox.setTag(false);
				holder.imgCheckBox.setImageResource(R.drawable.checkbox_normal);
			}

			holder.txtDisplayName.setText(contact.getName());
		}

		return row;
	}

	public void addAll(List<ContactDetail> data) {
		this.data = data;
	}

	static class ContactHolder {
		ImageView imgContactphoto;
		TextView txtDisplayName;
		TextView txtHiddenHeaderSegment;
		ImageView imgCheckBox;
	}

	public boolean clearImageCache() {
		if (imageLoader != null)
			imageLoader.clearCache();
		return true;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public ContactDetail getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	// SectionIndexer methods :
	@Override
	public int getPositionForSection(int section) {
		String c = mSections.charAt(section) + "";
		if (alphaIndexer.containsKey(c)) {
			return alphaIndexer.get(c);
		}
		return -1;
	}

	@Override
	public int getSectionForPosition(int position) {
		return 0;
	}

	@Override
	public Object[] getSections() {
		String[] sections = new String[mSections.length()];
		for (int i = 0; i < mSections.length(); i++)
			sections[i] = String.valueOf(mSections.charAt(i));
		return sections;
	}

	private class ContactAdapterContentObserver extends DataSetObserver {

		public synchronized void onChanged() {
			super.onChanged();
			int size = data.size();
			List<ContactDetail> tempData = data.subList(0, size);
			fillAlphaIndexer(tempData, NOT_PROCESS_WHOLE_LIST);
		}
	}

	public void fillAlphaIndexer(List<ContactDetail> tempData, byte identifier) {
		if (identifier == PROCESS_WHOLE_LIST) {
			int location = 0;
			int size = tempData.size();
			while (location < size) {
				ContactDetail contact = tempData.get(location);
				String name = contact.getName();
				int asciicode = (int) ((name.toLowerCase()).charAt(0));
				String s;
				if (!(asciicode >= 97 && asciicode <= 122)) {
					s = "#";
				} else {
					s = name.substring(0, 1).toUpperCase();
				}
				if (!alphaIndexer.containsKey(s)) {
					alphaIndexer.put(s, location);
				}
				location++;
			}
		} else {
			int location = tempData.size();
			if (location > 0) {
				location = location - 1;
				ContactDetail contact = tempData.get(location);
				String name = contact.getName();
				int asciicode = (int) ((name.toLowerCase()).charAt(0));
				String s;
				if (!(asciicode >= 97 && asciicode <= 122)) {
					s = "#";
				} else {
					s = name.substring(0, 1).toUpperCase();
				}

				if (!alphaIndexer.containsKey(s)) {
					alphaIndexer.put(s, location);
				}
			}
		}
	}

	public void unRegisterReciever() {
		if (observer != null)
			unregisterDataSetObserver(observer);
	}
}
