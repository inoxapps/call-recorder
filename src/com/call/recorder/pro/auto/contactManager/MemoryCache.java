package com.call.recorder.pro.auto.contactManager;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import android.graphics.Bitmap;

public class MemoryCache {

    //private static final String TAG = "MEMORY_CACHE";
    private Map<Object, Bitmap> cache=Collections.synchronizedMap(
            new LinkedHashMap<Object, Bitmap>());
    private long size=0;//current allocated size
    private long limit=1000000;//max memory in bytes

    public MemoryCache(){
        setLimit(Runtime.getRuntime().maxMemory()/4);
    }
    
    public void setLimit(long new_limit){
        limit=new_limit;
        //InoxLog.v(TAG, "MemoryCache will use up to "+limit/1024./1024.+"MB");
    }

    public Bitmap get(Object detail){
        try{
            if(!cache.containsKey(detail)){
            	//InoxLog.v(TAG, "cache has no image object.");
                return null;
            }
            //NullPointerException sometimes happen here http://code.google.com/p/osmdroid/issues/detail?id=78 
            Bitmap imageobj = cache.get(detail);
            //InoxLog.v(TAG, "cache has image object returning object..."+ imageobj);
            return imageobj;
        }catch(NullPointerException ex){
            ex.printStackTrace();
            return null;
        }
    }

    public void put(Object detail, Bitmap bitmap){
        try{
            if( cache.containsKey( detail ) )
                	size-=getSizeInBytes(cache.get(detail));
            	cache.put(detail, bitmap);
            	size+=getSizeInBytes(bitmap);
            	//InoxLog.v(TAG, "putting detail "+detail+" and bitmap object in cache. and size is "+size);
            	checkSize();
        }catch(Throwable th){
            th.printStackTrace();
        }
    }
    
    private void checkSize() {
    	//InoxLog.v(TAG, "cache size="+size+" length="+cache.size());
        if(size>limit){
            Iterator<Entry<Object, Bitmap>> iter=cache.entrySet().iterator();//least recently accessed item will be the first one iterated  
            while(iter.hasNext()){
                Entry<Object, Bitmap> entry=iter.next();
                size-=getSizeInBytes(entry.getValue());
                iter.remove();
                if(size<=limit)
                    break;
            }
            //InoxLog.v(TAG, "Clean cache. New size "+cache.size());
        }
    }

    public void clear() {
        try{
            //NullPointerException sometimes happen here http://code.google.com/p/osmdroid/issues/detail?id=78 
        	//InoxLog.v(TAG, "Clearing cache.");
            cache.clear();
            size=0;
        }catch(NullPointerException ex){
            ex.printStackTrace();
        }
    }

    long getSizeInBytes(Bitmap bitmap) {
        if(bitmap==null)
            return 0;
        return bitmap.getRowBytes() * bitmap.getHeight();
    }
}
