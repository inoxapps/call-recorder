package com.call.recorder.pro.auto.contactManager;

import java.io.IOException;
import java.io.InputStream;

import android.content.ContentUris;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.ContactsContract;

import com.call.recorder.pro.auto.utility.AppLog;

public class ContactDetail {

	private byte type;
	private String name;
	private String contactid;
	private ContactNumber numberObj;

	public class ContactNumber {

		public static final byte TYPE_HOME = 1;
		public static final byte TYPE_MOBILE = 2;
		public static final byte TYPE_WORK = 3;
		private byte type;
		private String number;

		public byte getType() {
			return type;
		}

		public void setType(byte type) {
			this.type = type;
		}

		public String getNumber() {
			return number;
		}

		public void setNumber(String number) {
			this.number = number;
		}
	}

	public class Address {
		private String pobox;
		private String street;
		private String neb;
		private String city;
		private String state;
		private String postalcode;
		private String country;
		private String type;

		public String getPobox() {
			return pobox;
		}

		public void setPobox(String pobox) {
			this.pobox = pobox;
		}

		public String getStreet() {
			return street;
		}

		public void setStreet(String street) {
			this.street = street;
		}

		public String getNeb() {
			return neb;
		}

		public void setNeb(String neb) {
			this.neb = neb;
		}

		public String getCity() {
			return city;
		}

		public void setCity(String city) {
			this.city = city;
		}

		public String getState() {
			return state;
		}

		public void setState(String state) {
			this.state = state;
		}

		public String getPostalcode() {
			return postalcode;
		}

		public void setPostalcode(String postalcode) {
			this.postalcode = postalcode;
		}

		public String getCountry() {
			return country;
		}

		public void setCountry(String country) {
			this.country = country;
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ContactNumber getNumberObj() {
		return numberObj;
	}

	public void setNumberObj(ContactNumber numberObj) {
		this.numberObj = numberObj;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public String getContactid() {
		return contactid;
	}

	public void setContactid(String contactid) {
		this.contactid = contactid;
	}

	public Bitmap getContactPhoto(Context context) throws IOException {
		Bitmap photo = null;
		if (contactid != null && !contactid.equals("")) {
			AppLog.i("getting contact image for : " + contactid+" : "+context.getContentResolver());
			Uri uri = ContentUris.withAppendedId(
					ContactsContract.Contacts.CONTENT_URI,
					Long.valueOf(contactid));
			AppLog.i("uri in getContactPhoto : "+uri);
			InputStream inputStream = ContactsContract.Contacts
					.openContactPhotoInputStream(context.getContentResolver(),
							uri);
			if (inputStream != null) {
				photo = BitmapFactory.decodeStream(inputStream);
				inputStream.close();
			}
		}
		return photo;
	}
}
