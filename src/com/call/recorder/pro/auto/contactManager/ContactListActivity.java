package com.call.recorder.pro.auto.contactManager;

import java.util.Timer;
import java.util.Vector;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.call.recorder.pro.auto.R;
import com.call.recorder.pro.auto.database.DBManager;
import com.call.recorder.pro.auto.preference.PrefrenceManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Globals;
import com.call.recorder.pro.auto.utility.Utilities;

public class ContactListActivity extends Activity {

	private IndexableListView listview;
	private EditText searchText;
	private TextView headerText;
	private ContactsArrayAdapter adapter;
	private ContactLoader contactLoader;
	Timer timer = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contactlist_frag_layout);

		getWindow().setSoftInputMode(
				WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		
		// RelativeLayout rladview = (RelativeLayout) findViewById(R.id.rlAdViewContacts);
		// AdMediation.showBanner(this, rladview);
		
		if (PrefrenceManager.isContactListChange(getApplicationContext())) {
			AppLog.i("Warning : Contact list changed clearing contact list.");
			PrefrenceManager.setContactChanged(getApplicationContext(), false);
			Globals.contactDetail.clear();
		}
		
		headerText = (TextView) findViewById(R.id.tvheadertext);
		Typeface tf = Typeface.createFromAsset(getApplicationContext()
				.getAssets(), "SCRIPTBL.TTF");
		headerText.setTextSize(20);
		headerText.setTypeface(tf);
		headerText.setText("Contacts");

		listview = (IndexableListView) findViewById(R.id.listcontroller)
				.findViewById(R.id.contactlistView);

		adapter = new ContactsArrayAdapter(getApplicationContext(),
				R.layout.contacts_listview_item, Globals.contactDetail);
		
		if (Globals.ASYNCH_STATUS == Globals.ASYNCH_NOT_ABLE_NOTIFY
				|| Globals.ASYNCH_STATUS == Globals.ASYNCH_COMPLETED_WITHOUT_NOTIFYING) {
			AppLog.i("asynch was not notifying the change so notify the change with adapter. ");
			adapter.notifyDataSetChanged();
		}

		listview.setAdapter(adapter);
		listview.setFastScrollEnabled(true);

		listview.setOnScrollListener(new OnScrollListener() {
			@Override
			public void onScroll(AbsListView view1, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				TextView text = (TextView) findViewById(R.id.listcontroller)
						.findViewById(R.id.headerTextSegment);
				if (Globals.contactDetail.size() > 0) {
					ContactDetail detail = Globals.contactDetail
							.get(firstVisibleItem);
					if (detail != null) {
						String headerText = "";
						char first = detail.getName().toLowerCase().charAt(0);
						int asciicode = (int) (first);
						if (!(asciicode >= 97 && asciicode <= 122)) {
							headerText = "#";
						} else {
							headerText = (first + "").toUpperCase();
						}
						text.setText(headerText);
					}
				}
			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
			}
		});
		
		if (Globals.contactDetail.size() == 0) {
			contactLoader = new ContactLoader(this, adapter, listview);
			contactLoader.execute();
		}

		listview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int index,
					long arg3) {
				ImageView imgview = (ImageView) view.findViewById(R.id.ivCheckbox);
				boolean clickStatus = (Boolean) imgview.getTag();
				AppLog.i("Default check value : "+clickStatus);
				DBManager manager = new DBManager();
				ContactDetail contact = Globals.contactDetail.get(index);
				String id = contact.getContactid();
				if(clickStatus){
					imgview.setTag(false);
					imgview.setImageResource(R.drawable.checkbox_normal);
					manager.removeContactFromRecording(Integer.parseInt(id));
				}
				else{
					imgview.setTag(true);
					imgview.setImageResource(R.drawable.checkbox_selected);
					manager.insertContactForRecording(Integer.parseInt(id));
				}
				manager = null;
			}
		});
		
		searchText = (EditText) (findViewById(R.id.editText1));
		searchText.addTextChangedListener(new TextWatcher() {
			private Vector<ContactDetail> filteredList = null;
			private String lastStringLen = "";

			@Override
			public void afterTextChanged(Editable arg0) {
				if (arg0.toString() != null && arg0.toString().length() > 0) {
					if (arg0.toString().length() < lastStringLen.length()) {
						filteredList.clear();
						filteredList = null;
					} else {
						if (!lastStringLen.equals(arg0.toString().substring(0,
								lastStringLen.length()))) {
							// InoxLog.v("check", "laststringlen != arg0");
							filteredList.clear();
							filteredList = null;
						}
					}
					filteredList = Utilities.getContactsFromString(
							arg0.toString(), Globals.contactDetail,
							filteredList);
					lastStringLen = arg0.toString();
					adapter = new ContactsArrayAdapter(getApplicationContext(),
							R.layout.contacts_listview_item, filteredList);
					listview.setAdapter(adapter);
				} else {
					adapter = new ContactsArrayAdapter(getApplicationContext(),
							R.layout.contacts_listview_item,
							Globals.contactDetail);
					listview.setAdapter(adapter);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {

			}
		});
	}

	@Override
	public void onPause() {
		super.onPause();
		adapter.clearImageCache();
	}

//	public Vector<ContactDetail> getList(Vector<ContactDetail> list) {
//		Vector<ContactDetail> tempList = new Vector<ContactDetail>();
//		int i = 0;
//		int size = list.size();
//		while (i < size) {
//			tempList.add(list.get(i));
//			i++;
//		}
//		return tempList;
//	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		adapter.unRegisterReciever();
	}

}
