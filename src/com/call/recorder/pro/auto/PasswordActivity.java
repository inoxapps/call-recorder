package com.call.recorder.pro.auto;

import com.call.recorder.pro.auto.preference.PrefrenceManager;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class PasswordActivity extends Activity{
	
	private int RESULT = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		final String password = PrefrenceManager.isOpenAppWithPassword(getApplicationContext());
		
		final Dialog dialog = new Dialog(this);
		dialog.setContentView(R.layout.entertextdialog);
		dialog.setTitle("Enter password.");
		final EditText passText = (EditText) dialog.findViewById(R.id.etPassText);
		Button okbtn = (Button) dialog.findViewById(R.id.btnOk);
		Button cancelbtn =(Button) dialog.findViewById(R.id.btnCancel);
		okbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String text = passText.getText().toString();
				if(text != null && text.length() > 3){
					if(text.equals(password)){
						RESULT = RecordingMainActivity.PASSWORD_RESULT;
						dialog.dismiss();
						finish();
					}
					else
						Toast.makeText(getApplicationContext(), "Incorrect password.", Toast.LENGTH_LONG).show();
				}
				else
					Toast.makeText(getApplicationContext(), "Password length should be greater than 3.", Toast.LENGTH_LONG).show();
			}
		});
		cancelbtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				RESULT = -1;
				dialog.dismiss();
				finish();
			}
		});
		dialog.show();
	}
	
	@Override
	public void finish() {
		if (getParent() == null) {
			setResult(RESULT, null);
		} else {
			getParent().setResult(RESULT, null);
		}
		super.finish();
	}
}
