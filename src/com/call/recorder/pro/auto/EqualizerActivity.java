package com.call.recorder.pro.auto;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnErrorListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.media.audiofx.Equalizer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class EqualizerActivity extends Activity{
	
	public final static String CONTACT_ID = "contactid";
	public final static String PLAY_FILE_NAME = "playfilename";
	
	public final static int API_LEVEL = 8;
	private int apilevel = 0;
	
	private  RelativeLayout mRelativeLayout; 
	Equalizer mEqualizer = null;
	MediaPlayer mMediaPlayer = null;
	
	ImageView ivStopButton;
    ImageView ivPauseButton;
    ImageView ivPlayButton;
    ImageView ivListButton;
    
	int progress = 1;
	int maxduration = 0;
	SeekBar playerSeek;
	Timer seekTimer;
	boolean isStopped = false;
	int contactid = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		if(intent == null ){
			AppLog.i("ERROR : EqualizerActivity has null intent. finishing...");
			finish();
		}
		
		apilevel = android.os.Build.VERSION.SDK_INT;
		 
		String filepath = intent.getStringExtra(PLAY_FILE_NAME);
		contactid = intent.getIntExtra(CONTACT_ID, -1);
		
		if(filepath == null){
			AppLog.i("ERROR : EqualizerActivity received no filename. finishing...");
			finish();
		}
		
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		
		mRelativeLayout = new  RelativeLayout(this); 
		mRelativeLayout.setBackgroundColor(Color.parseColor("#2b2b2b"));
        setContentView(mRelativeLayout); 
		
		try {
			mMediaPlayer = new MediaPlayer();
			String path = Utilities.getBasePath(getApplicationContext());
			if(path == null){
				AppLog.i("ERROR : finishing activity");
				finish();
			}
//			path = path + filename;
			AppLog.i("Playing file : "+filepath);
			mMediaPlayer.setDataSource(filepath);
			
			mMediaPlayer.setOnCompletionListener(new  MediaPlayer.OnCompletionListener() { 
	            @TargetApi(Build.VERSION_CODES.GINGERBREAD)
				public  void  onCompletion(MediaPlayer mediaPlayer) {
	            	AppLog.i("Media player file playing completed.");
	            	if(apilevel>API_LEVEL)
	            		mEqualizer.setEnabled(false); 
	            	ivPlayButton.setVisibility(View.VISIBLE);
					ivPauseButton.setVisibility(View.INVISIBLE);
//	            	mMediaPlayer.start();
	            } 
	        });
			
			mMediaPlayer.setOnErrorListener(new OnErrorListener() {
				@Override
				public boolean onError(MediaPlayer mp, int what, int extra) {
					AppLog.i("ERROR : Error occured in media player resetting media player.");
					ivPlayButton.setVisibility(View.VISIBLE);
					ivPauseButton.setVisibility(View.INVISIBLE);
					mp.reset();
					return true;
				}
			});
			
			mMediaPlayer.setOnPreparedListener(new OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mp) {
					AppLog.i("Media player prepared starting media player.");
					mMediaPlayer.start();
				}
			});
			
			mMediaPlayer.prepare();
			
//			if(apilevel<API_LEVEL)
//				setUpUserBitmap();			// default layout applying in absence of equalizer availability.
//			else
				setupEqualizerFxAndUI();
			
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private ImageView getUserBitmap() throws IOException{
        ImageView userImageView = new ImageView(this);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, 
        		RelativeLayout.LayoutParams.WRAP_CONTENT);
//      params.addRule(RelativeLayout.CENTER_IN_PARENT);
        userImageView.setLayoutParams(params);
        
        AppLog.i("INFO : Setting image of : "+contactid);
        if(contactid<1){
        	// TODO apply stub image.
        	userImageView.setImageDrawable(getResources().getDrawable(R.drawable.button_normal));
        }
        else{
        	Bitmap bitmap = null;
        	try {
        		bitmap = Utilities.getContactPhoto(getApplicationContext(), contactid);
			} catch (Exception e) {
				e.printStackTrace();
			}
        	userImageView.setImageBitmap(bitmap);
        }
        return userImageView;
	}
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private  void  setupEqualizerFxAndUI() throws IOException { 
	        // Create the Equalizer object (an AudioEffect subclass) and attach it to our media player,
	        // with a default priority (0).
		
			LinearLayout mLinearLayout = new  LinearLayout(this); 
	        mLinearLayout.setOrientation(LinearLayout.VERTICAL); 
        
	        if(apilevel>API_LEVEL){
		        mEqualizer = new  Equalizer(0, mMediaPlayer.getAudioSessionId()); 
		        mEqualizer.setEnabled(true); 
		 
		        TextView eqTextView = new  TextView(this); 
		        eqTextView.setText("Equalizer"); 
		        Typeface tf = Typeface.createFromAsset(getApplicationContext().getAssets(),
		       	        "SCRIPTBL.TTF");
		        eqTextView.setTypeface(tf);
		        eqTextView.setTextColor(Color.WHITE);
		        eqTextView.setPadding(5, 5, 5, 5);
		        eqTextView.setTextSize(20);
		        mLinearLayout.addView(eqTextView); 
		 
		        short  bands = mEqualizer.getNumberOfBands(); 
		        AppLog.i("Checking number of bands : "+bands);
		 
		        final  short  minEQLevel = mEqualizer.getBandLevelRange()[0]; 
		        final  short  maxEQLevel = mEqualizer.getBandLevelRange()[1]; 
		 
		        for  (short  i = 0; i < bands; i++) { 
		            final  short  band = i; 
		 
		            TextView freqTextView = new  TextView(this); 
		            freqTextView.setLayoutParams(new  ViewGroup.LayoutParams( 
		                    ViewGroup.LayoutParams.MATCH_PARENT, 
		                    ViewGroup.LayoutParams.WRAP_CONTENT)); 
		            freqTextView.setGravity(Gravity.CENTER_HORIZONTAL); 
		            freqTextView.setText((mEqualizer.getCenterFreq(band) / 1000) + " Hz"); 
		            freqTextView.setTextColor(Color.parseColor("#FFFFFF"));
		            mLinearLayout.addView(freqTextView); 
		 
		            LinearLayout row = new  LinearLayout(this); 
		            row.setOrientation(LinearLayout.HORIZONTAL); 
		 
		            TextView minDbTextView = new  TextView(this); 
		            LinearLayout.LayoutParams params = new  LinearLayout.LayoutParams( 
		            		LinearLayout.LayoutParams.WRAP_CONTENT, 
		            		LinearLayout.LayoutParams.WRAP_CONTENT);
		            params.setMargins(10, 0, 0, 0);
		            minDbTextView.setLayoutParams(params); 
		            minDbTextView.setTextColor(Color.parseColor("#FFFFFF"));
		            minDbTextView.setText((minEQLevel / 100) + " dB"); 
		 
		            TextView maxDbTextView = new  TextView(this); 
		            LinearLayout.LayoutParams params2 =  new  LinearLayout.LayoutParams( 
		            		LinearLayout.LayoutParams.WRAP_CONTENT, 
		            		LinearLayout.LayoutParams.WRAP_CONTENT);
		            params2.setMargins(0, 0, 1, 0);
		            maxDbTextView.setLayoutParams(params2); 
		            maxDbTextView.setTextColor(Color.parseColor("#FFFFFF"));
		            maxDbTextView.setText((maxEQLevel / 100) + " dB"); 
		 
		            LinearLayout.LayoutParams layoutParams = new  LinearLayout.LayoutParams( 
		                    ViewGroup.LayoutParams.MATCH_PARENT, 
		                    ViewGroup.LayoutParams.WRAP_CONTENT); 
		            layoutParams.weight = 1;
		            SeekBar bar = new  SeekBar(this); 
		            bar.setLayoutParams(layoutParams); 
		            bar.setMax(maxEQLevel - minEQLevel); 
		            bar.setThumb(getResources().getDrawable(R.drawable.equalizer_pointer));
		            bar.setProgressDrawable(getResources().getDrawable(R.drawable.band_progress));
		            bar.setProgress(-10);
		            bar.setProgress(mEqualizer.getBandLevel(band)); 
		 
		            bar.setOnSeekBarChangeListener(new  SeekBar.OnSeekBarChangeListener() { 
		                public  void  onProgressChanged(SeekBar seekBar, int  progress, 
		                        boolean  fromUser) { 
		                	try{
		                		mEqualizer.setBandLevel(band, (short) (progress + minEQLevel)); 
		                	}
		                	catch(Exception e){
		                		e.printStackTrace();
		                		finish();
		                	}
		                } 
		 
		                public  void  onStartTrackingTouch(SeekBar seekBar) {} 
		                public  void  onStopTrackingTouch(SeekBar seekBar) {} 
		            }); 
		 
		            row.addView(minDbTextView); 
		            row.addView(bar); 
		            row.addView(maxDbTextView); 
		 
		            mLinearLayout.addView(row); 
		        }
			}
	        else{
	        	ImageView view = getUserBitmap();
	        	mLinearLayout.addView(view);
	        }
	        
	        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            final RelativeLayout inflatedView = (RelativeLayout) inflater.inflate(R.layout.medialplayer, null);
            
            startSeekPlayer(inflatedView);
            
            ivStopButton = (ImageView) inflatedView.findViewById(R.id.ivstop);
            ivPauseButton = (ImageView) inflatedView.findViewById(R.id.ivpause);
            ivPlayButton = (ImageView) inflatedView.findViewById(R.id.ivplay);
            ivListButton = (ImageView) inflatedView.findViewById(R.id.ivList);
            
            ivStopButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					stopPlayer();
				}
			});
            
            ivPauseButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try{
						if(mMediaPlayer.isPlaying())
							mMediaPlayer.pause();
							seekTimer.cancel();
							ivPlayButton.setVisibility(View.VISIBLE);
							ivPauseButton.setVisibility(View.INVISIBLE);
					}
					catch(Exception e){
						e.printStackTrace();
						finish();
					}
				}
			});
            
            ivPlayButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					try{
						AppLog.i("media player playing state : "+mMediaPlayer.isPlaying());
						if(mMediaPlayer.isPlaying()){
							mMediaPlayer.stop();
							seekTimer.cancel();
							playerSeek.setProgress(0);
							progress = 1;
							isStopped = true;
						}
						
						if(isStopped){
							try {
								mMediaPlayer.prepare();
								mMediaPlayer.seekTo(0);
								AppLog.i("current position : "+mMediaPlayer.getCurrentPosition());
								isStopped = false;
							} catch (IllegalStateException e) {
								isStopped = false;
								e.printStackTrace();
							} catch (IOException e) {
								isStopped = false;
								e.printStackTrace();
							}
						}	
						
						mMediaPlayer.start();
						startSeekPlayer(inflatedView);
						
						ivPlayButton.setVisibility(View.INVISIBLE);
						ivPauseButton.setVisibility(View.VISIBLE);
					}
					catch(Exception e){
						e.printStackTrace();
						finish();
					}
				}
			});
            
            ivListButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					finish();
				}
			});
            
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.setMargins(5, 5, 5, 10);
            mRelativeLayout.addView(inflatedView, params);
            
            RelativeLayout.LayoutParams params2 = new RelativeLayout.LayoutParams( RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT );
            params2.addRule(RelativeLayout.ALIGN_PARENT_TOP);
            params2.setMargins(5, 10, 2, 2);
            if(apilevel>API_LEVEL)
            	mLinearLayout.setBackgroundResource(R.drawable.upper_bg);
            mRelativeLayout.addView(mLinearLayout, params2);
	    }
	
	public void startSeekPlayer(View inflatedView){
		playerSeek = (SeekBar) inflatedView.findViewById(R.id.sbPlayer);
        int duration = mMediaPlayer.getDuration();
        int secduration = duration / 1000;
        if(secduration > 0){
        	maxduration = secduration * 2;
        	playerSeek.setMax(maxduration);			// setting max to multiply by two because timer is going to execute in every 500ms.
        }
        
        AppLog.i("checking secduration : "+secduration + " : maxduration : "+maxduration);
        
        final Handler seekUpdateHandler = new Handler();
        seekTimer = new Timer();
        seekTimer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				seekUpdateHandler.post(new Runnable() {
					@Override
					public void run() {
						if(progress <= maxduration){
							AppLog.i("setting progress : "+progress);
							playerSeek.setProgress(progress);
							progress++;
						}
						if(progress > maxduration){
							AppLog.i("cancelling progress timer.");
//							mMediaPlayer.stop();
//							seekTimer.cancel();
//							playerSeek.setProgress(0);
							stopPlayer();
							// TODO make visible or invisible buttons in completionlistener.
							ivPlayButton.setVisibility(View.VISIBLE);
							ivPauseButton.setVisibility(View.INVISIBLE);
						}
					}
				});
			}
		}, 0, 500);
	}
	
	public void stopPlayer(){
		try{
			if(mMediaPlayer.isPlaying()){
				mMediaPlayer.stop();
				AppLog.i("current position in stop: "+mMediaPlayer.getCurrentPosition());
				seekTimer.cancel();
				progress = 1;
				playerSeek.setProgress(0);
				ivPlayButton.setVisibility(View.VISIBLE);
				ivPauseButton.setVisibility(View.INVISIBLE);
				isStopped = true;
			}
		}
		catch(Exception e){
			e.printStackTrace();
			finish();
		}
	}
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	protected void onStop() {
		super.onStop();
		if(mMediaPlayer != null){
			if(apilevel>API_LEVEL){
				if(mEqualizer != null)
					mEqualizer.release(); 
			}
			mMediaPlayer.stop();
			mMediaPlayer.release();
			mMediaPlayer = null;
		}
		seekTimer.cancel();
	}
	
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	protected void onDestroy() {
		if(mMediaPlayer != null){
			if(apilevel>API_LEVEL){
				if(mEqualizer != null)
					mEqualizer.release(); 
			}
			mMediaPlayer.release();
		}
		super.onDestroy();
	}

}
