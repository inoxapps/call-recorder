package com.call.recorder.pro.auto.database;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Application;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.call.recorder.pro.auto.database.DaoMaster.DevOpenHelper;
import com.call.recorder.pro.auto.utility.AppLog;

import de.greenrobot.dao.QueryBuilder;
import de.greenrobot.dao.WhereCondition;

public class DBManager extends Application {

	public final static byte CALL_TYPE_OUTGOING = 1;
	public final static byte CALL_TYPE_INCOMING = 2;

	private static SQLiteDatabase db;
	private static DaoMaster daoMaster;
	private static DaoSession daoSession;
	private static ContactTableDao contactdao;
	private static RecordContactsDao contactrecordingdao;

	@Override
	public void onCreate() {
		super.onCreate();
		DevOpenHelper helper = new DaoMaster.DevOpenHelper(this,
				"callrecorder-db", null);
		db = helper.getWritableDatabase();
		daoMaster = new DaoMaster(db);
		daoSession = daoMaster.newSession();
		contactdao = daoSession.getContactTableDao();
		contactrecordingdao = daoSession.getRecordContactsDao();

	}

	public int getRecordingCount() {
		ArrayList<ContactTable> list = (ArrayList<ContactTable>) contactdao
				.queryBuilder().list();
		if (list == null) {
			return -1;
		}
		int size = list.size();
		AppLog.i("DBManager return list size : " + size);
		return size;
	}

	public void storeData(int contactid, String contactnumber, byte state,
			long date, long duration, String filepath) {
		ContactTable ct = new ContactTable(null, contactid, contactnumber,
				state, date, duration, filepath);
		contactdao.insert(ct);
	}

	public ContactTable[] getData(int contactid) {
		WhereCondition cond = ContactTableDao.Properties.Contactid
				.eq(contactid);
		if (cond == null) {
			return null;
		}
		QueryBuilder<ContactTable> qb = contactdao.queryBuilder().where(cond);
		if (qb == null) {
			return null;
		}
		ArrayList<ContactTable> list = (ArrayList<ContactTable>) qb.list();
		if (list == null) {
			return null;
		}
		Iterator<ContactTable> itr = list.iterator();
		ContactTable item[] = new ContactTable[list.size()];
		int i = 0;
		while (itr.hasNext()) {
			item[i] = (ContactTable) itr.next();
			i++;
		}
		return item;
	}

	public ArrayList<ContactTable> getData() {
		ArrayList<ContactTable> list = (ArrayList<ContactTable>) contactdao
				.queryBuilder().list();
		if (list == null || list.size() < 0) {
			return null;
		}
		return list;
	}

	public ArrayList<ContactTable> getUnknownContactIds() {
		// TODO make query in which only contactid should select which is
		// greater than zero and group by contact id also.
//		List<ContactTable> list = contactdao.queryRaw(" GROUP BY contactid");
		List<ContactTable> list = contactdao.loadAll();
		if (list == null || list.size() < 0) {
			return null;
		}
		AppLog.i("queried list size in getContactIds : " + list.size());
		Iterator<ContactTable> itr = list.iterator();
		ArrayList<ContactTable> items = new ArrayList<ContactTable>();
		while (itr.hasNext()) {
			ContactTable contact = (ContactTable) itr.next();
			AppLog.i("INFO : in DBManager.getcontactids contact id : "
					+ contact.getContactid());
			if (contact.getContactid() < 0)
				items.add(contact);
		}
		return items;
	}

	public ArrayList<Integer> getContactIds() {
		// TODO make query in which only contactid should select which is
		// greater than zero and group by contact id also.
		List<ContactTable> list = contactdao.queryRaw(" GROUP BY contactid");
		if (list == null || list.size() < 0) {
			return null;
		}
		AppLog.i("queried list size in getContactIds : " + list.size());
		Iterator<ContactTable> itr = list.iterator();
		ArrayList<Integer> items = new ArrayList<Integer>();
		while (itr.hasNext()) {
			ContactTable contact = (ContactTable) itr.next();
			AppLog.i("INFO : in DBManager.getcontactids contact id : "
					+ contact.getContactid());
			if (contact.getContactid() > 0)
				items.add(contact.getContactid());
		}
		return items;
	}

	public ArrayList<ContactTable> getRecordingsFromId(int id) {
		ArrayList<ContactTable> list = (ArrayList<ContactTable>) contactdao
				.queryBuilder()
				.where(ContactTableDao.Properties.Contactid.eq(id)).list();
		if (list == null || list.size() < 0) {
			return null;
		}
		return list;
	}

	 public ArrayList<String> deleteData() {
//		 recordingdao.deleteAll();
		 List<ContactTable> contacts = contactdao
					.loadAll();
		 if(contacts == null || contacts.size() <= 0)
			 return null;
		 ArrayList<String> pathList = new ArrayList<String>();
		 Iterator<ContactTable> itr = contacts.iterator();
		 while(itr.hasNext()){
			 ContactTable contact = (ContactTable) itr.next();
			 pathList.add(contact.getFilepath());
			 contactdao.delete(contact);
		 }
		 return pathList;
	 }

	public boolean isContactSelected(Context context, String id) {
		try {
			AppLog.i("checking contact id for : "+id);
			List<RecordContacts> contacts = contactrecordingdao
					.queryBuilder().list();
			Iterator<RecordContacts> itr = contacts.iterator();
			while(itr.hasNext()){
				AppLog.i("Checking : "+((RecordContacts) itr.next()).getContactid());
			}
			RecordContacts list =  contactrecordingdao
					.queryBuilder()
					.where(RecordContactsDao.Properties.Contactid.eq(id))
					.unique();
			if (list == null) {
				return false;
			}
//			AppLog.i("checking selected contact list : "+list.size());
//			if (list.size() <= 0)
//				return false;
//			else
				return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public void insertContactForRecording(int id) {
		try {
			RecordContacts contact = new RecordContacts(null, id);
			contactrecordingdao.insert(contact);
			AppLog.i("inserted id is : "+id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void removeContactFromRecording(int id) {
		try {
			RecordContacts contact = contactrecordingdao.queryBuilder()
					.where(RecordContactsDao.Properties.Contactid.eq(id))
					.unique();
			if (contact != null){
				contactrecordingdao.delete(contact);
				AppLog.i("deleted id is : "+id);
			}
			else
				AppLog.i("ERROR : Delete operation fail for id : " + id);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String deleteRecording(String contactnumber, String filepath) {
		try {
//			ContactTable contact = recordingdao
//					.queryBuilder()
//					.where(ContactTableDao.Properties.Contactnumber
//							.eq(contactnumber),
//							ContactTableDao.Properties.Filepath.eq(filepath))
//					.unique();
			ContactTable contact = contactdao
					.queryBuilder()
					.where(ContactTableDao.Properties.Filepath.eq(filepath))
					.unique();
			if (contact != null) {
				contactdao.delete(contact);
				return contact.getFilepath();
			} else
				AppLog.i("ERROR : Delete operation fail for contactnumber : "
						+ contactnumber + " and filepath is " + filepath);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<String> deleteContactRecording(int id) {
		try {
			ArrayList<String> listOfFiles = new ArrayList<String>();
			List<ContactTable> contactlist = contactdao.queryBuilder()
					.where(ContactTableDao.Properties.Contactid.eq(id)).list();
			for (ContactTable contact : contactlist) {
				listOfFiles.add(contact.getFilepath());
				contactdao.delete(contact);
			}
			return listOfFiles;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public ArrayList<String> deleteKnownRecordings() {
		ArrayList<ContactTable> list = (ArrayList<ContactTable>) contactdao.queryBuilder().where(ContactTableDao.Properties.Contactid.gt(0)).list();
		ArrayList<String> listOfFiles = new ArrayList<String>();
		Iterator<ContactTable> iterator = list.iterator();
		while(iterator.hasNext()){
			ContactTable item = (ContactTable) iterator.next();
			listOfFiles.add(item.getFilepath());
			contactdao.delete(item);
		}
		return listOfFiles;
	}
	
	public ArrayList<String> deleteUnknownRecordings() {
		ArrayList<ContactTable> list = (ArrayList<ContactTable>) contactdao.queryBuilder().where(ContactTableDao.Properties.Contactid.le(0)).list();
		ArrayList<String> listOfFiles = new ArrayList<String>();
		Iterator<ContactTable> iterator = list.iterator();
		while(iterator.hasNext()){
			ContactTable item = (ContactTable) iterator.next();
			listOfFiles.add(item.getFilepath());
			contactdao.delete(item);
		}
		return listOfFiles;
	}

}
