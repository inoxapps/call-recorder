package com.call.recorder.pro.auto;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.call.recorder.pro.auto.preference.PrefrenceManager;
import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;

public class RecordingMainActivity extends FragmentActivity {
//	implements AdEventListener {

	private SharedPreferences sharedPrefs;
	private ImageView ivAppOnOff = null;
	private ImageView ivFragmentSwitcher = null;
	private TextView tvHeader = null;
//	private HtmlAd htmlAd = null;
	private UnknownContactFragment unknownFragment = null;
	private KnownContactFragment knownFragment = null;

	public static final byte KNOWN_CONTACT_FRAGMENT = 1;
	public static final byte UNKNOWN_CONTACT_FRAGMENT = 2;
	public static byte FRAGMENT_IDENTIFIER = UNKNOWN_CONTACT_FRAGMENT;

	public final static int PASSWORD_RESULT = 201;
	public final static int CONTACT_RECORDING_INFO_RESULT = 202;

	@Override
	protected void onStart() {
		super.onStart();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		//WorkThrough screen
		sharedPrefs = PreferenceManager
				.getDefaultSharedPreferences(getApplicationContext());
		AppLog.i("help screen check : "+sharedPrefs.getBoolean("firstHelpScreen", false));
		if (!sharedPrefs.getBoolean("firstHelpScreen", false)) {
			startActivity(new Intent(this, HelpScreenActivity.class));
			sharedPrefs.edit().putBoolean("firstHelpScreen", true).commit();
		}

		// ad mediation banner show.
		// RelativeLayout rladview = (RelativeLayout) findViewById(R.id.rlAdView);
		// AdMediation.showBanner(this, rladview);

		boolean isSimReady = Utilities.isSimPresent(getApplicationContext());
		if (!isSimReady) {
			Toast.makeText(getApplicationContext(), "Sim state unknown.",
					Toast.LENGTH_LONG).show();
		}
		
		// TODO Testing alarm functionality.
//		AppLog.i("Testing alarm");
//		PrefrenceManager.setDeleteRecordingTime(getApplicationContext(), RecorderAlarm.default_time_five_mins);
//		AppLog.i("alarm already set for time : "+PrefrenceManager.getDeleteRecordingTime(getApplicationContext()));
//		RecorderAlarm alarm = new RecorderAlarm();
//		alarm.SetAlarm(getApplicationContext());

		ivFragmentSwitcher = (ImageView) findViewById(R.id.ivFragmentSwitcher);
		ivFragmentSwitcher.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (FRAGMENT_IDENTIFIER == KNOWN_CONTACT_FRAGMENT)
					FRAGMENT_IDENTIFIER = UNKNOWN_CONTACT_FRAGMENT;
				else
					FRAGMENT_IDENTIFIER = KNOWN_CONTACT_FRAGMENT;
				showFragment();
			}
		});

		ivAppOnOff = (ImageView) findViewById(R.id.ivAppOnOff);
		ivAppOnOff.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				boolean app = PrefrenceManager
						.getAppStatus(getApplicationContext());
				if (app) {
					PrefrenceManager.setAppStatus(getApplicationContext(),
							false);
					ivAppOnOff.setImageResource(R.drawable.off_normal);
				} else {
					PrefrenceManager
							.setAppStatus(getApplicationContext(), true);
					ivAppOnOff.setImageResource(R.drawable.on_normal);
				}
			}
		});

		if (PrefrenceManager.getAppMode(getApplicationContext()) == PrefrenceManager.APP_MODE_SELECTED)
			FRAGMENT_IDENTIFIER = KNOWN_CONTACT_FRAGMENT;
		readyView();

		if (PrefrenceManager.isPassActive(getApplicationContext())
				&& PrefrenceManager
						.isOpenAppWithPassword(getApplicationContext()) != null) {
			startActivityForResult(new Intent(this, PasswordActivity.class),
					PASSWORD_RESULT);
		}
	}

	public void readyView() {
		tvHeader = (TextView) findViewById(R.id.tvHeader);
		Typeface tf = Typeface.createFromAsset(getApplicationContext()
				.getAssets(), "SCRIPTBL.TTF");
		tvHeader.setTextSize(20);
		tvHeader.setTypeface(tf);
//		DBManager manager = new DBManager();
//		ArrayList<ContactTable> li = manager.getData();
		tvHeader.setText("Recordings");

		showFragment();
	}

	public void initializeFragments() {
		if (knownFragment == null) {
			knownFragment = new KnownContactFragment();
		}
		if (unknownFragment == null) {
			unknownFragment = new UnknownContactFragment();
		}
	}

	public void showFragment(byte identifier) {
		if (identifier == KNOWN_CONTACT_FRAGMENT)
			FRAGMENT_IDENTIFIER = identifier;
		else
			FRAGMENT_IDENTIFIER = UNKNOWN_CONTACT_FRAGMENT;
		showFragment();
	}

	public void showFragment() {
		try{
			initializeFragments();
			FragmentTransaction transaction = getSupportFragmentManager()
					.beginTransaction();
			if (FRAGMENT_IDENTIFIER == KNOWN_CONTACT_FRAGMENT) {
				ivFragmentSwitcher.setImageResource(R.drawable.menu_normal);
				transaction.replace(R.id.fragment_container, knownFragment);
			} else {
				ivFragmentSwitcher.setImageResource(R.drawable.list_hover);
				transaction.replace(R.id.fragment_container, unknownFragment);
			}
			transaction.commit();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		AppLog.i("OnActivityResult : resultCode:" + resultCode
				+ "  :: requestCode" + requestCode);
		if (resultCode == RESULT_OK ){
			switch (requestCode) {
			case PASSWORD_RESULT :
				AppLog.i("PASSWORD INCORRECT");
				finish();
				break;
			case CONTACT_RECORDING_INFO_RESULT:
				showFragment();
				break;
			case ContactRecordings.RESULT_LIST_EMPTY:
				if(knownFragment.lastClickedIndex > 0){
					knownFragment.idList.remove(knownFragment.lastClickedIndex);
					knownFragment.adapter.notifyDataSetChanged();
				}
				break;
			case ContactRecordings.RESULT_LIST_NOT_EMPTY:
				break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onResume() {
		boolean app = PrefrenceManager.getAppStatus(getApplicationContext());
		if (app)
			ivAppOnOff.setImageResource(R.drawable.on_normal);
		else
			ivAppOnOff.setImageResource(R.drawable.off_normal);

		AppLog.i("Checking recording source : "
				+ PrefrenceManager.getRecordingSource(getApplicationContext()));
		super.onResume();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.settings:
			startActivity(new Intent(getApplicationContext(),
					SettingActivity.class));
			return true;
		case R.id.close:
			finish();
			return true;
		case R.id.deleteAllRec:
			if(FRAGMENT_IDENTIFIER == KNOWN_CONTACT_FRAGMENT){
				knownFragment.deleteAllRecordings();
			}
			else{
				unknownFragment.deleteAllRecordings();
			}
			return true;
		}
		return false;
	}
	
//	public void openEqualizer(View view) {
//		Intent equalizerintent = new Intent(this, EqualizerActivity.class);
//		equalizerintent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		equalizerintent
//				.putExtra(EqualizerActivity.PLAY_FILE_NAME, "record.3gp");
//		startActivity(equalizerintent);
//	}

	@Override
	public void onBackPressed() {
		try{
			// AdMediation.showBackInterstitial();
			super.onBackPressed();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
}
