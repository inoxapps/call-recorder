package com.call.recorder.pro.auto;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.call.recorder.pro.auto.utility.AppLog;
import com.call.recorder.pro.auto.utility.Utilities;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.ImageView;

public class ContactImageDisplayer {

	private Context context;
	private Handler handler = new Handler();// handler to display images in UI thread
	private ExecutorService executorService;
	private Map<Integer, Bitmap> bitmapViews = Collections
			.synchronizedMap(new LinkedHashMap<Integer, Bitmap>());
	private Map<Integer, ImageView> imageViews = Collections
			.synchronizedMap(new LinkedHashMap<Integer, ImageView>());
	private int stub_id = R.drawable.contactbox_small;
	
	public ContactImageDisplayer(Context context) {
		this.context = context;
		executorService = Executors.newFixedThreadPool(20);
	}
	
	public void DisplayImage(int id, ImageView imageView) {
		boolean imageSetFlag = false;
		if (imageViews.containsKey(id)) {
			Bitmap bmp = bitmapViews.get(id);
			if(bmp != null){
				imageView.setImageBitmap(bmp);
				imageSetFlag = true;
			}
			else
				imageSetFlag = false;
		}
		if (!imageSetFlag) {
			AppLog.i("Setting image");
			PhotoToLoad p = new PhotoToLoad(id, imageView);
			executorService.submit(new PhotosLoader(p));
		}
	}
	
	// Task for the queue
			private class PhotoToLoad {
				public int id;
				public ImageView imageView;

				public PhotoToLoad(int idd, ImageView i) {
					id = idd;
					imageView = i;
				}
			}

			class PhotosLoader implements Runnable {
				PhotoToLoad photoToLoad;

				PhotosLoader(PhotoToLoad photoToLoad) {
					this.photoToLoad = photoToLoad;
				}

				@Override
				public void run() {
					try {
//						if (imageViewReused(photoToLoad)) {
//							return;
//						}

						Bitmap bmp = getBitmap(photoToLoad.id);
						if (bmp == null) {
							bmp = BitmapFactory.decodeResource(
									ContactImageDisplayer.this.context.getResources(), stub_id);
						}

						BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
						handler.post(bd);
					} catch (Throwable th) {
						th.printStackTrace();
					}
				}
			}
			
			private Bitmap getBitmap(int id) throws IOException {
				Bitmap b = null;
					b = Utilities.getContactPhoto(ContactImageDisplayer.this.context,
							id);

				if (b != null) {
					return b;
				}
				return null;
			}

			boolean imageViewReused(PhotoToLoad photoToLoad) {
				ImageView imageview = imageViews.get(photoToLoad.id);
				if (imageview == null)
					return true;
				return false;
			}

			// Used to display bitmap in the UI thread
			class BitmapDisplayer implements Runnable {
				Bitmap bitmap;
				PhotoToLoad photoToLoad;

				public BitmapDisplayer(Bitmap b, PhotoToLoad p) {
					bitmap = b;
					photoToLoad = p;
				}

				public void run() {
					if (bitmap != null) {
						//InoxLog.v(TAG, "image contact photo is setting");
						photoToLoad.imageView.setImageBitmap(bitmap);
					} else {
						photoToLoad.imageView.setImageResource(stub_id);
					}
				}
			}
			
}
